import random, os, ast
from sage.all import factorial, binomial
from daglib import *

def shuffle(a,b):
    r = []
    na = len(a)
    nb = len(b)
    ia = 0
    ib = 0

    while ia < na and ib < nb:
        if (na - ia) / float(na + nb - ia - ib) < random.random():
            r.append(a[ia])
            ia += 1
        else:
            r.append(b[ib])
            ib += 1

    r = r + a[ia:] + b[ib:]
    return r


class FJ(object):
    def __init__(self):
        self.dag =DAG()
        self.top = self.dag.add_vertex()
        self.b = self.top
        self.n = 0
        self.dag_built = False
        
    def count_le(self):
        raise NotImplementedError

    def sample_le(self):
        raise NotImplementedError

    def make_dag(self):
        raise NotImplementedError

    def to_dag(self):
        if not self.dag_built :
            return self.make_dag()
        else:
            return self.dag
    
    def __len__(self):
        return self.n
    
class fullFJ(FJ):
    
    def __init__(self, left, right, bottom):
        super(fullFJ, self).__init__()
        
        self.left = left
        self.right = right
        self.bottom = bottom

        self.b = self.bottom.b
        
        self.n = 1 + len(left) + len(right) + len(bottom)

        
    def count_le(self):
        b_le = self.bottom.count_le()
        l_le = self.left.count_le()
        r_le = self.right.count_le()
        l = len(self.left)
        r = len(self.right)
               
        return b_le * (binomial(l+r,l)*l_le*r_le)

    
    def sample_le(self):
        b_le = self.bottom.sample_le()
        l_le = self.left.sample_le()
        r_le = self.right.sample_le()
        
        return [self.top] + shuffle(l_le,r_le) + b_le

    def make_dag(self):
        
        l_dag = self.left.to_dag()
        r_dag = self.right.to_dag()
        b_dag = self.bottom.to_dag()

        self.dag.vertices =  self.dag.vertices | l_dag.vertices | r_dag.vertices | b_dag.vertices

        self.dag.add_edge(self.top, self.left.top)
        self.dag.add_edge(self.top, self.right.top)

        self.dag.add_edge(self.left.b, self.bottom.top)
        self.dag.add_edge(self.right.b, self.bottom.top)

        self.dag_built = True
        
        return self.dag

class atomicFJ(FJ):

    def __init__(self):
        super(atomicFJ, self).__init__()
        self.dag_built = True
        self.n = 1

    def count_le(self):
        return 1

    def sample_le(self):
        return [self.top]

    def make_dag(self):
        return self.dag

class pendingFJ(FJ):
    
    def __init__(self, bottom):
        super(pendingFJ, self).__init__()
        self.bottom = bottom
        self.b = self.bottom.b
        self.n = 1 + len(self.bottom)

    def count_le(self):
        return self.bottom.count_le()

    def sample_le(self):
        return [self.top] + self.bottom.sample_le()

    def make_dag(self):
        b_dag = self.bottom.to_dag()

        self.dag.vertices = self.dag.vertices | b_dag.vertices

        self.dag.add_edge(self.top, self.bottom.top)

        self.dag_built = True
        
        return self.dag    

FILE_ID = 0

def randomFJ(size):
    global FILE_ID
    DAGVertex.NODE_ID = 0
    filename = "fjs/fj-"+str(FILE_ID)
    FILE_ID += 1    
    os.system("arbogen/bin/arbogen.native -v 0 -otype py "+
              "-o {} -min {} -max {} fj.spec 1>/dev/null".format(
                  filename, int(0.9*size), int(1.1*size)))

    d = dict()
    execfile(filename+".py", globals(), d)

    return d["t0"]
    
