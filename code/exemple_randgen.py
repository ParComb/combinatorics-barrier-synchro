from sage.all import SR, integral, var, RR
import random

t = var('t')

def root(pol,a,b):
    eq = pol == 0
    roots = eq.roots(t, ring=RR, multiplicities=False)
    return [root for root in roots if root >= a and root <= b][0]

random.seed(1)

intl = []

x = var('x1')
a = SR(0)
b = var('x2')
f = SR(1)
i = integral(f,x,a,b)
intl.append((f,x,a,b))

x = var('x5')
a = var('x3')
b = var('x8')
f = i
i = integral(f,x,a,b)
intl.append((f,x,a,b))

x = var('x7')
a = var('x4')
b = var('x8')
f = i
i = integral(f,x,a,b)
intl.append((f,x,a,b))

x = var('x8')
a = var('x6')
b = SR(1)
f = i
i = integral(f,x,a,b)
intl.append((f,x,a,b))

x = var('x6')
a = var('x3')
b = SR(1)
f = i
i = integral(f,x,a,b)
intl.append((f,x,a,b))

x = var('x3')
a = var('x4')
b = SR(1)
f = i
i = integral(f,x,a,b)
intl.append((f,x,a,b))

x = var('x4')
a = var('x2')
b = SR(1)
f = i
i = integral(f,x,a,b)
intl.append((f,x,a,b))

x = var('x2')
a = SR(0)
b = SR(1)
f = i
i = integral(f,x,a,b)
intl.append((f,x,a,b))


Xis = dict()

while len(intl) > 0:

    U = random.random()
    f,x,a,b = intl.pop()

    a = a.subs(Xis)
    b = b.subs(Xis)
    f = f.subs(Xis)
    
    C = integral(f,x,a,b)
    pol = integral(f,x,a,t)
    Xis[x] = root(pol-U*C,a,b)

    print(str(x) + " : " + str(Xis[x]))


