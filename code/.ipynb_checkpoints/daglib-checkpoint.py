
# Computation of the number of
# linear extensions for a (large) sub-class
# of DAGs

class DAGVertex:
    NODE_ID = 1
    def __init__(self):
        self.id = DAGVertex.NODE_ID
        DAGVertex.NODE_ID += 1
        self.preds = set()
        self.nexts = set()

    def to_dot_vertex(self):
        return "  N{};".format(self.id)

    def to_dot_edges(self):
        return "\n".join("  N{0} -> N{1};".format(self.id, dst.id) for dst in self.nexts)

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self is other

    def __repr__(self):
        return "DAGVertex(id={})".format(self.id)

def compute_leaves_of_vertices(vertices):
    return { v for v in vertices if not v.nexts and len(v.preds) == 1 }

def compute_roots_of_vertices(vertices):
    return { v for v in vertices if not v.preds and len(v.nexts) == 1 }

def compute_singles_of_vertices(vertices):
    return { v for v in vertices if len(v.preds)==1 and len(v.nexts)==1 }

class DAG:
    def __init__(self):
        self.vertices = set()

    def add_vertex(self):
        vertex = DAGVertex()
        self.vertices.add(vertex)
        return vertex

    def remove_vertex(self, vertex):
        assert(vertex in self.vertices)
        
        for pred in vertex.preds:
            pred.nexts.remove(vertex)

        for succ in vertex.nexts:
            succ.preds.remove(vertex)

        self.vertices.remove(vertex)

    def add_edge(self, src, dst):
        assert(src in self.vertices)
        assert(dst in self.vertices)

        src.nexts.add(dst)
        dst.preds.add(src)

    def remove_edge(self, src, dst):
        assert(src in self.vertices)
        assert(dst in self.vertices)
        assert(dst in src.nexts)
        assert(src in dst.preds)

        src.nexts.remove(dst)
        dst.preds.remove(src)

    def __len__(self):
        return len(self.vertices)

    def copy(self):
        cdag = DAG() 

        # copy the vertices
        DAGVertex.NODE_ID = 0
        vert_map = dict() # vertice_map
        for vert in self.vertices:
            nvert = cdag.add_vertex()
            vert_map[vert] = nvert

        # copy the edges
        for vert in self.vertices:
            nvert = vert_map[vert]
            for vnext in vert.nexts:
                nvnext = vert_map[vnext]
                cdag.add_edge(nvert, nvnext)

        return cdag
                

    class PathMap:
        def __init__(self):
            self.map = dict()

        def new_vertex(self, vertex):
            assert (vertex not in self.map)

            ancestors = set()
            for pred in vertex.preds:
                for ancestor in self.map[pred]:
                    ancestors.add(ancestor)

            redundant_preds = set()
            for pred in vertex.preds:
                if pred in ancestors:
                    redundant_preds.add(pred)

            for pred in vertex.preds:
                ancestors.add(pred)

            self.map[vertex] = ancestors

            return redundant_preds

        def __repr__(self):
            return repr(self.map)

    def transitive_reduction(self):
        reduced = False
        vertex_stack = [ v for v in self.vertices if not v.preds ]

        path_map = DAG.PathMap()

        while vertex_stack:
            vertex = vertex_stack.pop()
            try:
                vertex.visited
            except:
                vertex.visited = 0
            
            vertex.visited += 1

            # import pdb ; pdb.set_trace() # BREAKPOINT
            
            #print("[DEBUG] vertex={0} visited={1}  nb_preds={2}".format(vertex, vertex.visited, len(vertex.preds)))
            assert (len(vertex.preds)==0 or vertex.visited <= len(vertex.preds))
            if len(vertex.preds) == 0 or vertex.visited == len(vertex.preds):
                del vertex.visited
                redundant_preds = path_map.new_vertex(vertex)
                for redundant_pred in redundant_preds:
                    #print("[REDUCTION] removing transitive edge N{0} -> N{1}".format(redundant_pred.id, vertex.id))
                    self.remove_edge(redundant_pred, vertex) # remove a transitive edge
                    reduced = True

                # put next vertices on the stack
                vertex_stack.extend(vertex.nexts)

        # end while

        return reduced # return True if some edge has been removed


    def roots_reduction(self):
        reduced = False
        roots = compute_roots_of_vertices(self.vertices)

        for root in roots:
            if len(root.nexts) == 1:
                #print("[REDUCTION] remove root N{}".format(root.id))
                self.remove_vertex(root)
                reduced = True

        return reduced

    def to_dot(self):
        str_vertices = "\n".join(vertex.to_dot_vertex() for vertex in self.vertices)
        str_edges = "\n".join(vertex.to_dot_edges() for vertex in self.vertices)
        return "digraph {\n" + str_vertices + "\n" + str_edges + "}\n"

    def to_dot_file(self, fname):
        f = open(fname, "w")
        f.write(self.to_dot())
        f.close()

        
class CopyDAG(DAG):
    def __init__(self,src_dag):
        DAG.__init__(self)
        self.src_dag = src_dag
        self.vertex_map = dict()
        self.copy_dag(src_dag)

    def fetch_vertex(self, src_vert):
        return self.vertex_map[src_vert]

    def add_copy_vertex(self, src_vert):
        vertex = DAG.add_vertex(self)
        self.vertex_map[src_vert] = vertex
        return vertex

    def add_copy_edge(self, src_pred, src_next):
        pred = self.vertex_map[src_pred]
        next = self.vertex_map[src_next]
        DAG.add_edge(self, pred, next)

    def copy_dag(self, src_dag):

        # copy the vertices
        DAGVertex.NODE_ID = 0
        for src_vert in src_dag.vertices:
            nvert = self.add_copy_vertex(src_vert)

        # copy the edges
        for vert in src_dag.vertices:
            for vnext in vert.nexts:
                self.add_copy_edge(vert, vnext)


def test_transitive_reduction(dag, prefix="dag"):
    dag.transitive_reduction()
    dag.to_dot_file(prefix + "-intransitive.dot")

    return dag
    

if __name__ == "__main__":
    dag1 = DAG()
    v1 = dag1.add_vertex()
    v2 = dag1.add_vertex()
    v3 = dag1.add_vertex()
    dag1.add_edge(v1,v2)
    dag1.add_edge(v2,v3)
    dag1.add_edge(v1,v3)
    #dag1.to_dot_file("dag1.dot")

    #test_transitive_reduction(dag1, "dag1")
    #test_full_reduction(dag1, "dag1")

    import os
    for fname in os.listdir("./task_dags"):
        if fname.startswith("n"):
            DAGVertex.NODE_ID = 1
            test_full_reduction_task_dag("task_dags/", fname)

    #test_full_reduction_task_dag("task_dags/", "n-20_f-0.2_d-0.2_r-0.8_c-1_j-2.1")

    # the minimal (?) bad case
    #test_full_reduction(create_dag_k22())
    
