from __future__ import print_function

import random, itertools
from sage.all import Poset, Permutation
from daglib import *
from RandLinExtSage import *

def dag_to_poset(dag):

    E = set()
    R = list()
    
    for vertex in dag.vertices:
        xi = vertex.id
        E.add(xi)
        for pred in vertex.preds:
            pi = pred.id
            E.add(pi)
            R.append((pi, xi))

    n = len(dag.vertices)
    return Poset((E,R)).relabel()


def bc_step(poset, R, p):
    n = len(poset)
    i = random.randint(0,n-2)
    c = random.randint(0,1)

    if c == 1:
        a = [j for j in range(p) if R[j] == i]
        b = [j for j in range(p) if R[j] == i + 1]

        candidates = filter(lambda (x,y) : poset.compare_elements(x,y) is None,
                            itertools.product(a,b))
        
        if len(candidates) > 0:
            x,y = candidates.pop()
            R[x] = i+1
            R[y] = i
        elif len(b) == 0 and len(a) > 0 :
            x = a.pop()
            R[x] = i+1
        elif len(a) == 0 and len(b) > 0:
            x = b.pop()
            R[x] = i

    if all(R[x] < n-1 for x in range(p)):
        p = p + 1 if p < n else p

    return (poset, R, p)


def finished(R):
    return sorted(R) == list(range(len(R)))

def inverseR(R):
    return [x-1 for x in Permutation([x+1 for x in R]).inverse()]

def Ft(x, T, seeds):
    for t in range(T,1):
        if t in seeds:
            random.setstate(seeds[t])
        _, R, _ = x
        # print(R)
        x = bc_step(*x)

    return x


def CFTPrec(T, x, seeds):
    
    poset, R, p = Ft(x, T, seeds)
    
    if finished(R):
        return (poset, R, p)
    else:
        seeds[2*T] = random.getstate()
        x0 = CFTPrec(2*T, x ,seeds)
        return Ft(x0, T, seeds)


def CFTP(poset, T=-1000):
    n = len(poset)
    random.seed()
    
    seeds = {T : random.getstate()}
    p = 0
    R = [n-1]*n
    x = poset, R, p

    _, R, _ = CFTPrec(T, x, seeds)
    return inverseR(R)

if __name__ == '__main__':
    random.seed()
    arch_dag = make_arch_dag(10,1)
    poset = dag_to_poset(arch_dag)

    with open("test.dot", "w") as f:
        f.write(poset.graphviz_string())
        
    start_time = clock()

    for i in range(2):
        R = CFTP(poset)
        print(R)

    end_time = clock()
    elapsed_time = end_time - start_time
    print("Sampling time: {} s".format(elapsed_time))


        
    
            
