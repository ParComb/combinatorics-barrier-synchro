#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
from time import time
import os, sys, signal, random
from decimal import Decimal

import daglib
from ImportTaskDag import import_task_dag

import RandLinExtSage as BIT
import random_gen_arch_generalise_v2 as Arch
import Huber, ForkJoin

from benchlib import *

# random.seed(0xdeadbeef)
random.seed(hash("tag bench"))
# random.seed(hash("ish"))

# sys.setrecursionlimit(10**6)
Arch.Mem = True
    
sizes = [] # [10,30,100]
timeout = 100
nb_dags = 10
    
# Times for dags of size in sizes
for size in sizes:
    for nb_archs in range(2,size//3,2):
        archs = [Arch.UnifGeneralArch(size, nb_archs) for _ in range(nb_dags)]
            
        bench_arch = [BenchArch(arch) for arch in archs]
        
        dags = [Arch.escalier2DAG(*arch) for arch in archs]

        bench_huber = [BenchHuber(dag) for dag in dags]
        bench_bit = [BenchBIT(dag) for dag in dags]
            
        counting_arch = CountingRunner(bench_arch, timeout)
        counting_bit, sampling_bit = WholeRunner(bench_bit, timeout)
        sampling_arch = SamplingRunner(bench_arch, timeout)
        
        counting_bit = (0,0,0,0,0,0)
        sampling_bit = (0,0,0,0,0)
        sampling_huber = (0,0,0,0,0)
    
        if size < 50 :
            counting_bit, sampling_bit = WholeRunner(bench_bit, timeout)
            sampling_huber = SamplingRunner(bench_huber, timeout)

        print("""           ------ Counting Arch ------
#graphs : {}, size : {}, #archs : {}, average #le : {}

algo, #handled, tmin, tmed, tmax, tavg
Arch, {}, {}, {}, {}, {}
BIT, {}, {}, {}, {}, {}
""".format(nb_dags, size, nb_archs, counting_arch[0], *(counting_arch[1:] + counting_bit[1:])))

        print("""           ------ Sampling Arch ------
#graphs : {}, size : {}, #archs : {}, average #le : {}

algo, #handled, tmin, tmed, tmax, tavg
Arch, {}, {}, {}, {}, {}
BIT, {}, {}, {}, {}, {}
Huber, {}, {}, {}, {}, {}
""".format(nb_dags, size, nb_archs, counting_arch[0], *(sampling_arch + sampling_bit + sampling_huber)))
        

# Biggest DAG handled for BIT and Huber

limit_bit = (0,0,0,0,0,0,0)
limit_huber = (0,0,0,0,0)
stop_bit = False
stop_huber = False


for size in range(30,101,5):
    if stop_bit and stop_huber:
        break

    for nb_archs in range(2,size//3,2):
        if stop_bit and stop_huber:
            break

        dag = Arch.UnifGeneralArch(size, nb_archs)
    
        barch = BenchArch(dag)

        dag = Arch.escalier2DAG(*dag)
        dag_size = len(dag)
        
        bhuber = BenchHuber(dag)
        bbit = BenchBIT(dag)
    
        if not stop_bit:
            resc = CountingRunner([bbit], timeout)
        
            if resc[0] != 0:
                res = SamplingRunner([bbit], timeout)
            else:
                stop_bit = True
            
            if not stop_bit and res[-1] != 0:
                htime = SamplingRunner([bhuber], timeout)
                archtime = SamplingRunner([barch], timeout, 3)
                carchtime = CountingRunner([barch], timeout)
                limit_bit = (dag_size, Decimal(carchtime[0]), resc[-1], res[-1], carchtime[-1], htime[-1], archtime[-1], nb_archs)
            else:
                stop_bit = True
            
            if not stop_huber:
                res = SamplingRunner([bhuber], timeout)
        
        if not stop_huber and res[-1] != 0:
            archtime = SamplingRunner([barch], timeout, 3)
            carchtime = CountingRunner([barch], timeout)
            limit_huber = (dag_size, nb_archs, Decimal(carchtime[0]), res[-1], carchtime[-1], archtime[-1])
        else:
            stop_huber = True
    
print("""           ------ Biggest Arch ------

algo, size, #arch, #le, BIT, Huber, Arch counting, Arch sampling
Bit counting, {}, {}, {:.4g}, {:.3g}, {:.3g}, {:.3g}, {:.3g}
BIT sampling,  {}, {}, {:.4g}, {:.3g}
Huber, {}, {}, {:.4g}, -, {:.3g}, {:.3g}, {:.3g}""".format(
    limit_bit[0], limit_bit[-1], limit_bit[1], limit_bit[2], limit_bit[5], limit_bit[4], limit_bit[6],
    limit_bit[0], limit_bit[-1], limit_bit[1], limit_bit[3],
    *limit_huber))

last = ""

for size in range(100, 10001, 20):
    arch = Arch.UnifGeneralArch(size, size//3)
    barch = BenchArch(arch)
    s = SamplingRunner([barch],timeout)
    if s == (0,0,0,0,0):
        break
    else :
        c = CountingRunner([barch],0)
        last = """Arch, {}, {}, {:.4g}, -, -, {:.3g}, {:.3g}""".format(size, size//3, Decimal(c[0]), c[-1], s[-1])
print(last)

    
