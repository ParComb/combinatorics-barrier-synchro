
import subprocess
import sys

from IPython.core.magic import (Magics, magics_class, cell_magic, line_magic)
from IPython.display import (SVG, Image)


def run_graphviz(*options):
    cmd_line = ['dot']
    cmd_line.extend(options)

    subprocess.check_call(cmd_line)

def show_dot(dot):
    filename = "graph.dot"

    with open(filename, 'w') as f:
        f.write(dot)

    run_graphviz(filename, '-T', 'svg', '-o', filename + ".svg")

    return SVG(filename=filename+".svg")
 
    
# The class MUST call this class decorator at creation time
@magics_class
class DotMagics(Magics):

    @cell_magic
    def dot(self, line, cell):
        #print("promela line = '{}'".format(line))

        filename = "graph.dot"
        line = line.strip()
        if line != '':
            filename = line
            
        with open(filename, 'w') as f:
            f.write(cell)

        #print("Generated dot file: '{}'".format(filename))
        #print("----")

        run_graphviz(filename, '-T', 'svg', '-o', filename + ".svg")

        return SVG(filename=filename+".svg")


def installation():

    # In order to actually use these magics, you must register them with a
    # running IPython.  This code must be placed in a file that is loaded once
    # IPython is up and running:
    ip = get_ipython()
    # You can register the class itself without instantiating it.  IPython will
    # call the default constructor on it.
    ip.register_magics(DotMagics)


if __name__ == "__main__":
    print("TODO : some tests ?")
