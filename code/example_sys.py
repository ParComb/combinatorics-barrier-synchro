from daglib import *
from RandLinExtSage import *
from sage.all import gp

from benchlib import *

def make_dag():
    DAGVertex.NODE_ID = 0

    dag = DAG()
    
    init = dag.add_vertex()
    gen = dag.add_vertex()
    yield1 = dag.add_vertex()
    yield2 = dag.add_vertex()
    end = dag.add_vertex()
    step1 = dag.add_vertex()
    load = dag.add_vertex()
    xform = dag.add_vertex()
    step4 = dag.add_vertex()
    step2 = dag.add_vertex()
    step3 = dag.add_vertex()
    fork = dag.add_vertex()
    comp1 = dag.add_vertex()
    join = dag.add_vertex()
    comp21 = dag.add_vertex()
    comp22 = dag.add_vertex()

    dag.add_edge(init, gen)
    dag.add_edge(init, step1)
    dag.add_edge(init, fork)
    dag.add_edge(gen, yield1)
    dag.add_edge(yield1, yield2)
    dag.add_edge(yield1, step3)
    dag.add_edge(yield2, end)
    dag.add_edge(step1, load)
    dag.add_edge(step1, step2)
    dag.add_edge(step2, step3)
    dag.add_edge(load, xform)
    dag.add_edge(xform, step4)
    dag.add_edge(step3, step4)
    dag.add_edge(step4, end)
    dag.add_edge(fork, comp1)
    dag.add_edge(fork, comp21)
    dag.add_edge(comp1, join)
    dag.add_edge(comp21, comp22)
    dag.add_edge(comp22, join)
    dag.add_edge(join, end)

    return dag
    
if __name__ == '__main__':
    dag = make_dag()
    size = len(dag)
    
    bhuber = [BenchHuber(dag)]
    bbit = [BenchBIT(dag)]

    c = CountingRunner(bbit,0)
    s = SamplingRunner(bbit,0,10)
    sh = SamplingRunner(bhuber,0,10)

    print("""------------ Example Sys -----------

algo, size, #le, time
BIT coutning, {}, {}, {}
BIT sampling, {}, {}, {}
Huber, {}, {}, {}
""".format(size, c[0], c[-1], size, c[0], s[-1], size, c[0], sh[-1]))
