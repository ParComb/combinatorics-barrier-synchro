#!/usr/bin/python
# -*- coding: utf-8 -*-

import math


def compte(n):
    return math.factorial(n)


def decompo(i):
    # decompose i en base factorielle
    # i = i_1 1! + i_2 2! + ... + i_n n!
    # avec   0 <= i_j <= j
    # preuve par rec n! = (n-1) * n! + n-1)!

    r = i
    L = []
    j = 1
    while r > 0:
        L.append( r % j )
        r = r // j
        j += 1
    return L

def compo(L):
    r = 0
    m = 1
    i = 0
    for e in L:
        r += e*m
        i += 1
        m *= i
    return r


def permutation_unrank_lex(r,n):
    L = [i for i in range(1,n+1)]
    LR = []
    D = decompo(r)
    D += [0]*(n-len(D))
    D.reverse()   
    for i in range(len(D)):
        LR.append(L[D[i]])
        L = L[:D[i]] + L[D[i]+1:]
    return LR + L[:]


def est_combi(L):
    t = -1
    for i in range(len(L)-1):
        if L[i] > L[i+1]:
            if t >-1:
                return (False,-1)
            else:
                t = i
    return (True, t+1)
        

def test(n):
    for i in range(compte(n)):
        U = permutation_unrank_lex(i,n)

        D = decompo(i)
        D += [0]*(n-len(D))

        rep, t = est_combi(U)
        if rep:
            print(D, '  ', i, '   :   ', U, '    ', t)
        
    return 0

def test(n, t):
    l = 0
    for i in range(compte(n)):
        U = permutation_unrank_lex(i,n)

        D = decompo(i)
        D += [0]*(n-len(D))
        
        rep, u = est_combi(U)
        if rep and (u==0 or u==t):
            V = cons(n,t,l)
            print(l, V,'-',i,D, '   :   permut ', U, '    ', t)
            if V != U:
                print('pb',n,t,i)
            l += 1

def total_test(n):
    for tt in range(1,n+1):
 #       print(math.factorial(n)//math.factorial(tt)//math.factorial(n-tt), '   ',n,tt, '  ', nb_croissant(n,tt,0))
        test(n,tt)
   #     print('   ')

def binom(n,k):
    global binomial
    if (n,k) in binomial:
        return binomial[(n,k)]
    b = compte(n)//compte(k)//compte(n-k)
    binomial[(n,k)] = b
    return b

##def nb_croissant(n,t,m):
##    global memo
##    #m valeur min
##
##   #normalement ce cas suffit
## #   if m==0:
##  #      return compte(n+1)//compte(t)//compte(n+1-t)
##
##    if (n,t,m) in memo:
##        return memo[(n,t,m)]
##    
##    if t == 0:
##        return 1
##    if m >= n-t+1:
##        return 0
##    L = [nb_croissant(n-1,t-1,i) for i in range(m,n)]
##    s = sum(L)
##    memo[(n,t,m)] = s
##    return s

def nb_croissant(n,t,m):
    return binom(n-m,t)

        
def unrank(n,t,m,r):
    if t == 0:
        return []
    s = r
    i = m
    while s > -1:
  #      print(n-1,t-1,i,nb_croissant(n-1,t-1,i))
        tmp = nb_croissant(n-1,t-1,i)
        s -= tmp
        i += 1
    i -= 1
    s += tmp
    return unrank(n-1,t-1,i,s) + [i]


def cons(n,t,i):
    U = unrank(n,t,0,i)
 #   print(U)
    L = [0] * (n-t) + U
    s = compo(L)
  #  print(L,s,n,t)
    return permutation_unrank_lex(s,n)

import time
import random

memo = dict()
binomial = dict()
pts = []

##for n in [300]:
##    for t in range(1,n//2+1,5):
##        C = math.factorial(n)//math.factorial(t)//math.factorial(n-t)
##        print('début du calcul de ',n,t)
##        for i in range(40):
##            r = random.randint(0, C)
##            tm = time.time()
##            cons(n,t,r)
##            pts.append([t,time.time()-tm])
##        print('fin du calcul de ',n,t)
##print(pts)

