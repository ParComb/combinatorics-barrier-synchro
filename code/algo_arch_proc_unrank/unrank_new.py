T = {}

def t(n,k):
    global T
    
    if k == 0:
        return 1
    
    if (n,k) in T:
        return T[(n,k)]

    T[(n,k)] = ( (n+2*k-1)*t(n,k-1) + (n-k)*t(n+1,k-1) ) // 2
    return T[(n,k)]

##
##
##def shift(N,K,U):
##    V = []
##    for i in range(1,len(U)):
##        a,b = U[i]
##        if a == 'a':
##            V.append('a'+str(int(b)-1))
##        elif a == 'x':
##            if b == '1':
##                V.append('a'+str(K))
##            else:
##                V.append('x'+str(int(b)-1))
##        elif a == 'c':
##            if b == '1':
##                V.append('x'+str(N-K))
##            else:
##                V.append('c'+str(int(b)-1))
##        else: # a=='b'
##            if b == '1':
##                V.append(a+str(K))
##            else:
##                V.append(a+str(int(b)-1))
##            
##    V.append('c'+str(K))
##    print('shift',U,V)
##    return V
##
##def shift2(N,K,k,pos,U):
##    V = []
##    for i in range(0,len(U)):
##        a,b = U[i]
##        if a == 'a' or a == 'c' or a == 'b':
##            V.append(a+b)
##        else:
##            if int(b) < pos:
##                V.append('x'+b)
##            elif int(b) == pos:
##                V.append('b'+str(K+1-k))
##            else:
##                V.append('x'+str(int(b)-1))
##    print('shift2',k,pos,U,V)
##    return V
##
##
##def unrank(N,K,n,k,r):
##
##    if k==0:
##        return ['a'+str(i) for i in range(1,K+1)] + ['x'+str(i) for i in range(1,n-K+1)] + ['c'+str(i) for i in range(1,K+1)]
##    
##    if r < (n+2*k-1) * t(n,k-1):
##        pos = r // t(n,k-1)
##        rr = r % t(n,k-1)
##        U = unrank(N,K,n,k-1,rr)
##        p = U.index('a'+str(K+1-k))
##        U = U[:p + 1 + pos] + ['b'+str(K+1-k)] + U[p+1+pos:]
##        if ('x'+str(K+1-k) in U and p+1+pos > U.index('x'+str(K+1-k))) or (p+1+pos > U.index('c'+str(K+1-k))):
##            U = shift(N,K,U)
##        return U
##    else:
##        r -= (n+2*k-1) * t(n,k-1)
##        pos = r // t(n+1,k-1)
##        rr = r % t(n+1,k-1)
##        U = unrank(N,K,n+1,k-1,rr)
##        U = shift2(N,K,k,pos+2,U) 
##        return U

def test(k,run):

    for i in range(1,k+1):
        if (run.index('a'+str(i)) > run.index('b'+str(i))) or (run.index('b'+str(i)) > run.index('c'+str(i))) :
            return False
    return True


##def shift(N,K,n,k,U):
##    V = []
##
##    for i in range(1,len(U)):
##        a,b = U[i]
##        if a == 'a':
##            V.append('a'+str(int(b)-1))
##        elif a == 'x':
##            if b == '1':
##                V.append('a'+str(k))
##            else:
##                V.append('x'+str(int(b)-1))
##        elif a == 'b':
##            V.append(U[i])
##        else:  # a == 'c'
##            if b == '1':
##                V.append('x'+str(n-k))
##            else:
##                V.append('c'+str(int(b)-1))
##    print('shift',U,V)
##    return V

def shift(N,K,n,k,U):
    V = []

    A = K-k
    C = K-k
    X = 0
    for i in range(1,len(U)):
        a,b = U[i]
        if a == 'a':
            A += 1
            V.append('a'+str(A))
        elif a == 'x':
            if b == '1':
                A += 1
                V.append('a'+str(A))
            else:
                X += 1
                V.append('x'+str(X))
        elif a == 'b':
            V.append(U[i])
        else:  # a == 'c'
            if b == '1':
                X += 1
                V.append('x'+str(X))
            else:
                C += 1
                V.append('c'+str(C))
    print('shift',U,V)
    return V

def shift2(K,k,pos,U):
    p = U.index('x1')
    V = U[:p+1+pos] + ['b'+str(K+1-k)]
 
    for i in range(p+2+pos, len(U)):
        a,b = U[i]
        V.append(U[i])
        if a == 'x':
            V[i] = 'x'+str(int(b)-1)
    print('shift2', pos, U, V)
    return V
        

def unrank(N,K,n,k,r):

    if k==0:
        return ['x'+str(i) for i in range(1,n+1)]
    
    if r < (n+2*k-1) * t(n,k-1):
        pos = r // t(n,k-1)
        rr = r % t(n,k-1)
        U = ['a'+str(K+1-k)] + unrank(N,K,n,k-1,rr) + ['c'+str(K+2-k)]
        p = U.index('x1')

        print(pos, U)           
        U = U[:pos+1] + ['b'+str(K+1-k)] + U[pos+1:]
        print(pos,p,U)
        if (pos >= p):
            U = shift(N,K,n,k,U)
        else:
            p = U.index('x'+str(n-k+1))
            U[p] = 'c'+str(K+1-k)
            U = U[:-1]
        print(U)
        return U
    else:
        r -= (n+2*k-1) * t(n,k-1)
        pos = r // t(n+1,k-1)
        rr = r % t(n+1,k-1)
        U = ['a'+str(K+1-k)] + unrank(N,K,n+1,k-1,rr)
        U = shift2(K,k,pos,U)
        return U





N=3
K=2
D = dict()
for i in range(2*t(N,K)):  #jusqu'à 6 OK
    u = unrank(N,K,N,K,i)
    if tuple(u) in D:
        D[tuple(u)].append(i)
    else:
        D[tuple(u)] = [i]
        
    print(u)
 #   print(test(K,u))
    print(' ')
for d in D:
    print(d, D[d])

