T = {}

def t(n,k):
    global T
    
    if k == 0:
        return 1
    
    if (n,k) in T:
        return T[(n,k)]

    T[(n,k)] = ( (n+2*k-1)*t(n,k-1) + (n-k)*t(n+1,k-1) ) // 2
    return T[(n,k)]

def factorial(n):
    if n == 0:
        return 1
    return n*factorial(n-1)

def bounds(n,k):

    return (factorial(n) // factorial(n-k), t(n,k), factorial(n+2*k-1)//factorial(n+k-1))


import random


def shift(U):
    V = []
    for i in range(len(U)):
        a = U[i][0]
        b = U[i][1:]
        if a != 'x':
            V.append( a + str(int(b)+1) )
        else:
            V.append( U[i] )
    return V


def deshift(U):
    V = []
    for i in range(len(U)):
        a = U[i][0]
        b = U[i][1:]
        V.append( a + str(int(b)-1))
    return V



def gen(n,k):
    
    if k == 0:
        return ['x'+str(i) for i in range(1,n+1)]

    r = random.randint(0, 2*t(n,k)-1)

    if r < (n+2*k-1) * t(n,k-1):
        U = gen(n,k-1)
        U = ['a1'] + shift(U) + ['c'+str(k+1)]
        pos = 1 + r // t(n,k-1)
        U = U[:pos] + ['b1'] + U[pos:]
        p = U.index('x1')
        if (p < pos):
            U[p] = 'a'+str(k+1)
            U[pos] = 'b'+str(k+1)
            U = deshift(U[1:])
        else:
            p = U.index('x'+str(n-k+1))
            U[p] = 'c1'
            U = U[:-1]
        return U

    else:
        U = gen(n+1,k-1)
        U = ['a1'] + shift(U)

        ind = 2 + (r- (n+2*k-1)*t(n,k-1)) // t(n+1,k-1)

        p = U.index('x'+str(ind))
        U[p] = 'b1'
        i = p+1
        while (U[i] != 'x'+str(n-k+2)):
            a = U[i][0]
            b = U[i][1:]
            if a == 'x':
                U[i] = 'x'+str(int(b)-1)
            i += 1
        U[i] = 'c1'
        return U




   
N = 5
K = 4 
D = dict()
for i in range(20000):

    G = tuple(gen(N,K))
    if G in D:
        D[G] += 1
    else:
        D[G] = 1

print(len(D), t(N,K))


