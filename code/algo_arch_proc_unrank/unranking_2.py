T = {}

def t(n,k):
    global T
    
    if k == 0:
        return 1
    
    if (n,k) in T:
        return T[(n,k)]

    T[(n,k)] = ( (n+2*k-1)*t(n,k-1) + (n-k)*t(n+1,k-1) ) // 2
    return T[(n,k)]


TT = {}

def tt(n,k,i):
    global TT

    if (k==0) and (n>0):
        if i == 1:
            return 1
        else:
            return 0
#    if (k-n==0) or (i<k+1) or (i>2*k+1):
#        return 0

    if (n,k,i) in TT:
        return TT[(n,k,i)]

    TT[(n,k,i)] = (i-2)*tt(n,k-1,i-2) + (n-k)*tt(n+1,k-1,i-1) 
    return TT[(n,k,i)]



import random


def shift(U):
    V = []
    for j in range(len(U)):
        a = U[j][0]
        b = U[j][1:]
        if a != 'x':
            V.append( a + str(int(b)+1) )
        else:
            V.append( U[j] )
    return V


def gen(n,k,l,r):
    
    if k == 0:
        print(k,l)
        return ['x'+str(i) for i in range(1,n+1)]
    
    if r < (l-2)*tt(n,k-1,l-2):
        pos = 1 + r // tt(n,k-1,l-2)
        rr = r % tt(n,k-1,l-2)
        U = gen(n,k-1,l-2,rr)

        U = ['a1'] + shift(U)
        U = U[:pos] + ['b1'] + U[pos:]
        
        p = U.index('x'+str(n-k+1))
        U[p] = 'c1'
        return U

    else:
        r -= (l-2)*tt(n,k-1,l-2)
        pos = 2 + r // tt(n+1,k-1,l-1)
        rr = r % tt(n+1,k-1,l-1)
        U = gen(n+1,k-1,l-1,rr)
        U = ['a1'] + shift(U)

        p = U.index('x'+str(pos))
        U[p] = 'b1'
        j = p+1
        while (U[j] != 'x'+str(n-k+2)):
            a = U[j][0]
            b = U[j][1:]
            if a == 'x':
                U[j] = 'x'+str(int(b)-1)
            j += 1
        U[j] = 'c1'
        return U

def main(n,k,r):
    l = k+1
    while r >=0:
        r -= tt(n,k,l)
        l += 1
    l -= 1
    r += tt(n,k,l)
    return gen(n,k,l,r)


def pos(k,U):
    return [1+U.index('x1')],[1+U.index('b'+str(i)) for i in range(1,K+1) ]

D = dict()

N = 5
K = 4
##for i in range(K+1,2*K+2):
##    for r in range(tt(N,K,i)):
##        G = tuple(gen(N,K,i,r))
###        print(r,i,G)
## #       print(pos(K,G))
##        if G in D:
##            D[G] += 1
##        else:
##            D[G] = 1
##
##print(len(D), t(N,K))
###for d in D:
###    print(d, D[d])


D = dict()

for r in range(t(N,K)):
    G = tuple(main(N,K,r))
    print(r, G)
    if G in D:
        D[G] += 1
    else:
        D[G] = 1

print(len(D), t(N,K))

