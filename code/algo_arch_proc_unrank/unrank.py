
T = {}

def t(n,k):
    global T
    
    if k == 0:
        return 1
    
    if (n,k) in T:
        return T[(n,k)]

    T[(n,k)] = ( (n+2*k-1)*t(n,k-1) + (n-k)*t(n+1,k-1) ) // 2
    return T[(n,k)]


def tt(n,k):
    global T
    
    if k == 0:
        return 1
    
    if (n,k) in T:
        return T[(n,k)]

    T[(n,k)] = ( (n+2*k-1)*t_points(n,k-1) + (n-k)*t_points(n+1,k-1) ) // 2
    return T[(n,k)]

pts = set()

def t_points(n,k):
    global pts

    if (n,k) not in pts:
        pts.add((n,k))
    return tt(n,k)


def I2(n,k):
    return (n-k)*t_points(n+1,k-1)







def rep(L):
    d = dict()
    for a in L:
        if tuple(a) in d:
            d[tuple(a)] += 1

        else:
            d[tuple(a)]=1
    return d


import random

#auhmenter taille stack
import sys
sys.setrecursionlimit(4*10**5)

##N = 2000
##K = 2000
##s = t(N,K)
##for i in range(10000):
##    unrank(N,K, random.randint(0,s-1))
##
##ch = '{'
##for pt in pts:
##    ch += str(pt) + ','
##ch = ch[:-1] + '}'
##
##fic = open("/Users/tan/Desktop/T.txt", "w")
##fic.write(ch)
##fic.close()


##t(2000,2000)
##print('calculs faits')
##print(t(2000,2000))
##M = []
##n = 2000
##for k in range(2000, 1, -1):
##    if (n+2*k-1) * T[(n,k-1)] >= (n-k) * T[(n+1,k-1)]:
##        M.append( (k-1, n) )
##    else:
##        n += 1
##        M.append( (k-1, n) )

##t(2000,2000)
##print('calculs faits')
##M = []
##n = 2000
##for k in range(2000, 1, -1):
##    print((k,n), (n+2*k-1) * T[(n,k-1)] / ( (n+2*k-1) * T[(n,k-1)] + (n-k) * T[(n+1,k-1)]))
##    if (n+2*k-1) * T[(n,k-1)] >= (n-k) * T[(n+1,k-1)]:
##        M.append( (k-1, n) )
##    else:
##        n += 1
##        M.append( (k-1, n) )


##def unrank(n,k,r):
##    global T
##
##    if k == 0:
##        return []
##
##    if r < I2(n,k):
##        p = r // t(n+1,k-1)
##        rr  = r % t(n+1,k-1)
##        pos = unrank(n+1,k-1,rr)
##        return [(True,p)] + pos
##    else:
##        R = r - I2(n,k)
##        p = R // t(n,k-1)
##        rr = R % t(n,k-1)
##        if (p==k) and (rr==0):
##            rr = t(n,k-1)-1
##        pos = unrank(n, k-1, rr)
##        return [(False,p)] + pos

##
##def unrank(n,k,r):
##    global T
##
##    if k == 0:
##        return []
##
##    if r < (n-k) * t(n+1,k-1):
##        p = r // t(n+1,k-1)
##        rr  = r % t(n+1,k-1)
##        pos = unrank(n+1,k-1,rr)
##        return [(True,p)] + pos
##    else:
##        R = r - (n-k)*t(n+1,k-1)
##        p = R // t(n,k-1)
##        rr = R % t(n,k-1)
##        pos = unrank(n,k-1,rr)
##        return [(False,p)] + pos
##
##
##def cons(K,n,k,pos,trunck):
##    if k == 0:
##        return trunck[:]
##
##    b,p = pos[0]
##    if b:
##        deb = trunck.index('a'+str(K))
##        trunck = trunck[:deb+1+p] + ['b'+str(K+1-k)] + trunck[deb+1+p:]
##        return cons(K,n+1,k-1,pos[1:],trunck)
##    else:
##        trunck = cons(K,n,k-1,pos[1:],trunck)
##        deb = trunck.index('a'+str(K+1-k))
##        return trunck[:deb+1+p] + ['b'+str(K+1-k)] + trunck[deb+1+p:]
##                
##
##def cons_run(n,k,r):
##    global T
##
##    pos = unrank(n,k,r)
##    trunck = ['a'+str(i) for i in range(1,k+1)] + ['x'+str(i) for i in range(1,n-k+1)] + ['c'+str(i) for i in range(1,k+1)] 
##    return cons(k,n,k,pos,trunck)
##
##
##
##def cons_run2(n,k,pos):
##    global T
##
##    trunck = ['a'+str(i) for i in range(1,k+1)] + ['x'+str(i) for i in range(1,n-k+1)] + ['c'+str(i) for i in range(1,k+1)] 
##    return cons(k,n,k,pos,trunck)
##
##
def test(k,run):

    for i in range(1,k+1):
        if (run.index('a'+str(i)) > run.index('b'+str(i))) or (run.index('b'+str(i)) > run.index('c'+str(i))) :
            return False
    return True
##
##
##N = 3
##K = 3
##L = [unrank(N,K,i) for i in range(t(N,K))]
##for i in range(len(L)):
##    if test(K,cons_run2(N,K,L[i])):
##        for j in range(len(L)):
##            if i!=j and cons_run2(N,K,L[i]) == cons_run2(N,K,L[j]):
##                print(L[i])
##                print(L[j])
##                print(cons_run2(N,K,L[i]))
##                print(' ')
##    else:
##        print('PB : ',L[i], cons_run2(N,K,L[i]))




def unrank(N,K,n,k,R):
    global T

    if k == 0:
        return ['a'+str(i) for i in range(1,K+1)] + ['x'+str(i) for i in range(1,N-K+1)] + ['c'+str(i) for i in range(1,K+1)]
    
    pos = R // t(n,k-1)
    r = R % t(n,k-1)
    U = unrank(N,K,n,k-1,r)
    p = U.index('c'+str(K+1-k))
    return U[:p + pos + 1] + ['b'+str(K+1-k)] + U[p + pos+1:]

