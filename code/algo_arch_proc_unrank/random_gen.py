T = {}

def t(n,k):
    global T
    
    if k == 0:
        return 1
    
    if (n,k) in T:
        return T[(n,k)]

    T[(n,k)] = ( (n+2*k-1)*t(n,k-1) + (n-k)*t(n+1,k-1) ) // 2
    return T[(n,k)]


import random


def shift(U):
    V = []
    for i in range(len(U)):
        a,b = U[i]
        if a != 'x':
            V.append( a + str(int(b)+1) )
        else:
            V.append( U[i] )
    return V


def deshift(U):
    V = []
    for i in range(len(U)):
        a,b = U[i]
        V.append( a + str(int(b)-1))
    return V



def gen(n,k):
    
    if k == 0:
        return ['x'+str(i) for i in range(1,n+1)]

    r = random.randint(0, 2*t(n,k)-1)

    if r < (n+2*k-1) * t(n,k-1):
        U = gen(n,k-1)
#        print(U)
        U = ['a1'] + shift(U) + ['c'+str(k+1)]
#        print('shift',U)
        pos = 1 + r // t(n,k-1)
#        pos = random.randint(1,n+2*k-1)
        U = U[:pos] + ['b1'] + U[pos:]
#        print(n+2*k-1, pos, len(U),U)
        p = U.index('x1')
 #       print(pos,U)
        if (p < pos):
            U[p] = 'a'+str(k+1)
            U[pos] = 'b'+str(k+1)
#            print(U)
            U = deshift(U[1:])
#            print('deshift',U)
        else:
            p = U.index('x'+str(n-k+1))
            U[p] = 'c1'
            U = U[:-1]
 #           print('coupe',U)
        return U

    else:
        U = gen(n+1,k-1)
#        print(U)
        U = ['a1'] + shift(U)
#        print('shift,cas 2',U)
        ind = 2 + (r- (n+2*k-1)*t(n,k-1)) // t(n+1,k-1)
#        ind = random.randint(2,n-k+1)
#        print('ind',ind)
        p = U.index('x'+str(ind))
        U[p] = 'b1'
        i = p+1
        while (U[i] != 'x'+str(n-k+2)):
 #           print(U)
            a,b = U[i]
            if a == 'x':
                U[i] = 'x'+str(int(b)-1)
            i += 1
        U[i] = 'c1'
        return U




def test(k,run):

    for i in range(1,k+1):
        if (run.index('a'+str(i)) > run.index('b'+str(i))) or (run.index('b'+str(i)) > run.index('c'+str(i))) :
            return False
    return True

def gen_exhaustive(K):
    base = ['a'+str(i) for i in range(1,K+1)] + ['c'+str(i) for i in range(1,K+1)]
    E = {tuple(base)}

    for i in range(1,K+1):
        E2 = set()
        for ee in E:
            e = list(ee)
            for p in range(e.index('a'+str(i))+1, e.index('c'+str(i))+1):
                f = e[:p] + ['b'+str(i)] + e[p:]
                E2.add(tuple(f))
        E = {e for e in E2}
    return E


   
N = 5
K = 4 
D = dict()
for i in range(20000):

    G = tuple(gen(N,K))
    if G in D:
        D[G] += 1
    else:
        D[G] = 1

print(len(D), t(N,K))


