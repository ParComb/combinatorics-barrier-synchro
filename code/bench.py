#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
from time import time
import os, random, signal, sys

# from sage.all import  integral, var, factorial, solve, SR, RR, Poset, Permutation, binomial

from sage.all import gp

import daglib
from ImportTaskDag import import_task_dag

import RandLinExtSage as BIT
import random_gen_arch_generalise_v2 as Arch
import Huber, ForkJoin

from sage.all import factorial

from benchlib import *

# class TimeoutException(Exception):
#     pass

# class Bench(object):

#     def __init__(self, graph):
#         self.graph = graph
#         self.pc = None # résultat des précalculs
    
#     def count(self):
#         raise NotImplementedError
        
#     def precalcul(self):
#         raise NotImplementedError

#     def sample(self):
#         # Utilise self.pc calculé par self.precalcul(graph)
#         raise NotImplementedError


# class BenchFJ(Bench):
#     def __init__(self, graph):
#         super(BenchFJ, self).__init__(graph)
        
#     def count(self):
#         return self.graph.count_le()
    
#     def sample(self):
#         return self.graph.sample_le()

# class BenchBIT(Bench):
#     def __init__(self, graph):
#         super(BenchBIT, self).__init__(daglib.CopyDAG(graph))

#     def count(self):       
#         BIT.INTEGRALS = []
#         poly, integrals = BIT.compute_INTEGRALS(self.graph)
#         self.pc = (integrals, poly.vars)
#         # print("GP poly : "+ poly.poly +" end GP")
#         # nb_le = gp(poly.poly +" * factorial("+str(len(poly.vars))+")").sage()
#         nb_le = BIT.count_linear_extensions(poly)

#         return nb_le
#         # return BIT.count_linear_extensions(self.pc)
        
#     def sample(self):
#         if self.pc is None:
#             raise TimeoutException
#         else:
#             return BIT.sample_linear_extension(*self.pc)

# class BenchHuber(Bench):
#     def __init__(self, graph):
#         super(BenchHuber, self).__init__(Huber.dag_to_poset(graph))

#     def sample(self):
#         return Huber.CFTP(self.graph)

# class BenchArch(Bench):
#     def __init__(self, graph):
#         super(BenchArch, self).__init__(graph[0])
#         self.T = dict()

#     # def precalcul(self):
        
#     #     Arch.preCalculs(self.graph)

#     def count(self):
#         Arch.T = self.T
#         res = Arch.enum(self.graph)
#         self.T = Arch.T
#         return res
        
#     def sample(self):
#         Arch.T = self.T
#         return Arch.alea_gen(Arch.fil(self.graph), self.graph, len(self.graph)-1)
    
# def handler(signum, frame):
#     raise TimeoutException("Too much time spent")

# def CountingRunner(benchs, timeout):

#     signal.signal(signal.SIGALRM, handler)
#     signal.alarm(timeout)
   
#     times = list()
#     t = time()
#     nb_le = 0
#     try:
#         for bench in benchs:
#             nb_le += bench.count()
#             times.append(time() - t)
#             t = time()
            
#     except TimeoutException:
#         pass
#     signal.alarm(0)

#     times.sort()

#     if len(times) == 0:
#         return (0,0,0,0,0,0)
        
#     tmin = times[0]
#     tmax = times[-1]
#     tmed = times[len(times)//2]
#     tavg = reduce(lambda a, b: a+b, times) / len(times)
    
#     le_avg = nb_le / float(len(times))
    
#     # avg le, nb handled, tmin, tmed, tmax, tavg
#     return (le_avg, len(times), tmin, tmed, tmax, tavg)

# TEST = 0

# def SamplingRunner(benchs, timeout, nb_samples=3):
#     global TEST
#     signal.signal(signal.SIGALRM, handler)
#     signal.alarm(timeout*nb_samples)
   
#     times = list()
#     t = time()

#     try:
#         for bench in benchs:
#             for _ in range(nb_samples):
#                 bench.sample()
#             times.append(time() - t)
#             t = time()
            
#     except TimeoutException:
#         pass
    
#     signal.alarm(0)

#     if len(times) == 0:
#         return (0,0,0,0,0)
    
#     times = map(lambda x: x/nb_samples, times)
#     times.sort()
    
#     tmin = times[0]
#     tmax = times[-1]
#     tmed = times[len(times)//2]
#     tavg = reduce(lambda a, b: a+b, times) / len(times)
        
#     # nb handled, tmin, tmed, tmax, tavg
#     return (len(times), tmin, tmed, tmax, tavg)

# def PrecalculRunner(benchs, timeout):

#     signal.signal(signal.SIGALRM, handler)
#     signal.alarm(timeout)
   
#     times = list()
#     t = time()

#     try:
#         for bench in benchs:
#             bench.precalcul()
#             times.append(time() - t)
#             t = time()
            
#     except TimeoutException:
#         pass
    
#     signal.alarm(0)

#     if len(times) == 0:
#         return (0,0,0,0,0,0)
    
#     times.sort()
    
#     tmin = times[0]
#     tmax = times[-1]
#     tmed = times[len(times)//2]
#     tavg = reduce(lambda a, b: a+b, times) / len(times)
        
#     # nb handled, tmin, tmed, tmax, tavg
#     return (len(times), tmin, tmed, tmax, tavg)



if __name__ == '__main__':

    
    random.seed(0xdeadbeef)

    timeout = 100
    
    # target_size = 100
    target_size = int(sys.argv[1])
    nb_arch = int(sys.argv[2])
    

    Arch.Mem = True

    # # dag = Arch.escalier2DAG(*Arch.UnifGeneralArch(target_size, random.randint(2,target_size//3)))
    # dag = Arch.escalier2DAG(*Arch.UnifGeneralArch(target_size, target_size//3))
    # # dag = ForkJoin.randomFJ(target_size).to_dag()
    # # dag = ForkJoin.randomFJ(50).to_dag()
    # b = BenchBIT(dag)
    # dag.to_dot_file("test")
    # print(b.count())
    # print(b.sample())

    # print("""------ Sampling Arch ------""")
    # for target_size in range(10,target_size+1,10):

    #     for nb_arch in range(2,target_size//3+1,2):

    # fj = ForkJoin.randomFJ(target_size)
    
    arch = Arch.UnifGeneralArch(target_size, nb_arch)
    arch2 = Arch.UnifGeneralArch(target_size, nb_arch)
    # print(arch)
    bench_arch = [BenchArch(arch), BenchArch(arch2)]

    # bench_fj = [BenchFJ(fj)]
            
    dag = Arch.escalier2DAG(*arch)
    dag2 = Arch.escalier2DAG(*arch2)

    dag.to_dot_file("test")
    
    # dag = fj.to_dag()
    bench_bit = [BenchBIT(dag)]
    bench_huber = [BenchHuber(dag)]
        
    # counting_fj = CountingRunner(bench_fj, timeout)
    counting_fj = CountingRunner(bench_arch, timeout)
    counting_bit = CountingRunner(bench_bit, timeout)
    sampling_bit = (0,0,0,0,0)
    if counting_bit[0] != 0:
        sampling_bit = SamplingRunner(bench_bit, timeout)
        sampling_huber = SamplingRunner(bench_huber, timeout)

    print("""
SIZE {}; NB LE BIT {}; NB LE ARCH {}

algo, time
BIT , {}
Huber , {}

#""".format(target_size, counting_bit[0], counting_fj[0], sampling_bit[1], sampling_huber[1]))

#     for target_size in range(10,target_size+1,10):
#         fj = ForkJoin.randomFJ(target_size)

#         bench_fj = [BenchFJ(fj)]
                
#         dag = fj.to_dag()
#         # bench_bit = [BenchBIT(dag)]
#         bench_huber = [BenchHuber(dag)]
        
#         counting_fj = CountingRunner(bench_fj, timeout)
#         # counting_bit = CountingRunner(bench_bit, timeout)
#         # sampling_bit = (0,0,0,0,0)
#         # if counting_bit[0] != 0:
#         #     sampling_bit = SamplingRunner(bench_bit, timeout)
#         sampling_huber = SamplingRunner(bench_huber, timeout)

#         print("""------ Sampling FJ ------
# #graphs : {} , size : {}, average #le : {}

# algo , #handled , tmin , tmed , tmax , tavg
# Huber , {}, {}, {}, {}, {}

# """.format(1, target_size, counting_fj[0], *sampling_huber))

        
    

    

    
#     ################## FJ ##################

#     fjs = [ForkJoin.randomFJ(target_size) for _ in range(1000)]
#     nb_graphs = len(fjs)
#     avg_size = reduce(lambda a, b: a+b, map(len, fjs)) / nb_graphs
       
#     bench_fj = [BenchFJ(fj) for fj in fjs]
    
#     fjs = [fj.to_dag() for fj in fjs]
    
#     bench_bit = [BenchBIT(fj) for fj in fjs]
#     bench_huber = [BenchHuber(fj) for fj in fjs]

#     counting_fj = CountingRunner(bench_fj, timeout)
#     counting_bit = CountingRunner(bench_bit, timeout)
#     # precalcul_bit = PrecalculRunner(bench_bit, timeout)
#     sampling_fj = SamplingRunner(bench_fj, timeout)
#     sampling_bit = SamplingRunner(bench_bit, timeout)
#     sampling_huber = SamplingRunner(bench_huber, timeout)
    
#     print("""------ Counting FJ ------
# #graphs : {} , average size : {} , average #le : {}

# algo , #handled , tmin , tmed , tmax , tavg
# FJ , {}, {}, {}, {}, {}
# BIT , {}, {}, {}, {}, {}

# """.format(nb_graphs, avg_size, counting_fj[0], *(counting_fj[1:] + counting_bit[1:])))
    
#     print("""------ Sampling FJ ------
# #graphs : {} , average size : {} , average #le : {}

# algo , #handled , tmin , tmed , tmax , tavg
# FJ , {}, {}, {}, {}, {}
# BIT , {}, {}, {}, {}, {}
# Huber , {}, {}, {}, {}, {}

# """.format(nb_graphs, avg_size, counting_fj[0], *(sampling_fj + sampling_bit + sampling_huber)))





    

#     ################## ARCH ##################
    
#     archs = [Arch.UnifGeneralArch(target_size, random.randint(2,target_size//3)) for _ in range(1000)]
    
#     bench_arch = [BenchArch(arch) for arch in archs]
    
#     archs = [Arch.escalier2DAG(*arch) for arch in archs]
#     nb_graphs = len(archs)
#     avg_size = reduce(lambda a, b: a+b, map(len, archs)) / nb_graphs

    
#     bench_bit = [BenchBIT(arch) for arch in archs]
#     bench_huber = [BenchHuber(arch) for arch in archs]

#     counting_arch = CountingRunner(bench_arch, timeout)
#     counting_bit = CountingRunner(bench_bit, timeout)
#     # precalcul_arch = PrecalculRunner(bench_arch, timeout)
#     sampling_arch = SamplingRunner(bench_arch, timeout)
#     sampling_bit = SamplingRunner(bench_bit, timeout)
#     sampling_huber = SamplingRunner(bench_huber, timeout)

    
#     print("""------ Counting Arch ------
# #graphs : {} , average size : {} , average #le : {}

# algo , #handled , tmin , tmed , tmax , tavg
# Arch , {}, {}, {}, {}, {}
# BIT , {}, {}, {}, {}, {}

# """.format(nb_graphs, avg_size, counting_arch[0], *(counting_arch[1:] + counting_bit[1:])))

#     print("""------ Sampling Arch ------
# #graphs : {} , average size : {} , average #le : {}

# algo , #handled , tmin , tmed , tmax , tavg
# Arch , {}, {}, {}, {}, {}
# BIT , {}, {}, {}, {}, {}
# Huber , {}, {}, {}, {}, {}

# """.format(nb_graphs, avg_size, counting_arch[0], *(sampling_arch + sampling_bit + sampling_huber)))



    


#     ################## BIT ##################

#     task_dags = list()

#     for filename in os.listdir("task_dags"):
#         daglib.DAGVertex.NODE_ID = 0
#         dag = daglib.DAG()
#         import_task_dag("task_dags/" + filename, dag)
#         if len(dag) < target_size + 20 and len(dag) > target_size - 20:
#             task_dags.append(dag)

#     random.shuffle(task_dags)
#     nb_graphs = len(task_dags)
#     avg_size = reduce(lambda a, b: a+b, map(len, task_dags)) / nb_graphs

    
#     bench_bit = [BenchBIT(dag) for dag in task_dags]
#     bench_huber = [BenchHuber(dag) for dag in task_dags]
    
#     counting_bit = CountingRunner(bench_bit, timeout)
#     sampling_bit = SamplingRunner(bench_bit, timeout)
#     sampling_huber = SamplingRunner(bench_huber, timeout)

    
#     print("""------ Counting BIT-free ------
# #graphs : {} , average size : {} , average #le : {}

# algo , #handled , tmin , tmed , tmax , tavg
# BIT , {}, {}, {}, {}, {}

# """.format(nb_graphs, avg_size, *counting_bit))

#     print("""------ Sampling BIT-free ------
# #graphs : {} , average size : {} , average #le : {}

# algo , #handled , tmin , tmed , tmax , tavg
# BIT , {}, {}, {}, {}, {}
# Huber , {}, {}, {}, {}, {}

# """.format(nb_graphs, avg_size, counting_bit[0], *(sampling_bit + sampling_huber)))
