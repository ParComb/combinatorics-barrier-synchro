#from __future__ import print_function

import utils
import daglib

from sage.all import integral, var, factorial

#from time import clock

from Syntax import *

VERBOSE = True

class Poly:
    def __init__(self, poly=None,vars=None):
        if poly is None:
            self.poly = 1
        else:
            self.poly = poly
        if vars is None:
            self.vars = set()
        else:
            self.vars = vars
            
    def single_elimination(self, node, pred, succ):
        nvar, pvar, svar = var('N'+str(node.id)) , var('N'+str(pred.id)), var('N'+str(succ.id)) 
        nvars = self.vars | {nvar, pvar, svar}
        if VERBOSE:
            print("[Single({})]: integral{{{}, {}}}({}).d{}".format(nvar, svar, pvar, self.poly, nvar))
        npoly = integral(self.poly, nvar, svar, pvar)
        if VERBOSE:
            print("         ==> {}\n".format(npoly))
        return Poly(poly=npoly, vars=nvars)
        
    def root_elimination(self, node, succ):
        nvar, svar = var('N'+str(node.id)), var('N'+str(succ.id)) 
        nvars = self.vars | {nvar, svar}
        if VERBOSE:
            print("[Root({})]: integral{{{}, 1}}({}).d{}".format(nvar, svar, self.poly, nvar))
        npoly = integral(self.poly, nvar, svar, 1)
        if VERBOSE:
            print("         ==> {}\n".format(npoly))
        return Poly(poly=npoly, vars=nvars)

    def leaf_elimination(self, node, pred):
        nvar, pvar = var('N'+str(node.id)) , var('N'+str(pred.id)) 
        nvars = self.vars | {nvar, pvar}
        if VERBOSE:
            print("[Leaf({})]: integral{{0, {}}}({}).d{}".format(nvar, pvar, self.poly, nvar))
        npoly = integral(self.poly, nvar, 0, pvar)
        if VERBOSE:
            print("         ==> {}\n".format(npoly))
        return Poly(poly=npoly, vars=nvars)

    def alone_elimination(self, node):
        nvar = var('N'+str(node.id)) 
        nvars = self.vars | {nvar}
        if VERBOSE:
            print("[Alone({})]: integral{{0, 1}}({}).d{}".format(nvar, self.poly, nvar))
        npoly = integral(self.poly, nvar, 0, 1)
        if VERBOSE:
            print("         ==> {}\n".format(npoly))
        return Poly(poly=npoly, vars=nvars)
        
    def __str__(self):
        return str(self.poly)
        
    def __repr__(self):
        return repr(self.poly)

def alone_reduction(poly,dag):
    alone = utils.set_take(dag.vertices)
    npoly = poly.alone_elimination(alone)
    dag.remove_vertex(alone)
    
    if dag.vertices:
        raise ValueError("Node not alone!")

    return npoly

def roots_reduction(poly,dag):
    roots = daglib.compute_roots_of_vertices(dag.vertices)
    reduced = False
    npoly = poly
    for root in roots:
        npoly = root_reduction(npoly, dag, root)
        reduced = True
    return reduced, npoly
    
def root_reduction(poly,dag,root):
    #if VERBOSE:
    #    print("[REDUCTION] remove root N{}".format(root.id))
    npoly = poly.root_elimination(root, utils.set_take(root.nexts))
    dag.remove_vertex(root)

    return npoly

def leaves_reduction(poly,dag):
    leaves = daglib.compute_leaves_of_vertices(dag.vertices)
    reduced = False
    npoly = poly
    for leaf in leaves:
        npoly = leaf_reduction(npoly, dag, leaf)
        reduced = True
        
    return reduced, npoly

def leaf_reduction(poly,dag,leaf):
    #if VERBOSE:
    #    print("[REDUCTION] remove leaf N{}".format(leaf.id))
    npoly = poly.leaf_elimination(leaf, utils.set_take(leaf.preds))
    dag.remove_vertex(leaf)

    return npoly

def singles_reduction(poly,dag):
    singles = daglib.compute_singles_of_vertices(dag.vertices)
    reduced = False
    npoly = poly
    for single in singles:
        npoly = single_reduction(npoly, dag, single)
        reduced = True
    return reduced, npoly
    
def single_reduction(poly,dag,single):
    #if VERBOSE:
    #    print("[REDUCTION] remove single N{}".format(single.id))
    next = utils.set_take(single.nexts)
    pred = utils.set_take(single.preds)
    npoly = poly.single_elimination(single, pred , next)
    dag.add_edge(pred, next)
    dag.remove_vertex(single)

    return npoly

def count_linear_extensions(dag):
    dag_len = len(dag.vertices)
    reduced = True
    poly = Poly()
    while reduced:
        reduced = False
        #print("  .. Transitive reduction .. ",end='')
        red1 = dag.transitive_reduction()
        # if red1:
        #     print("Done")
        # else:
        #     print("None")
        # print("  .. Roots reduction .. ",end='')
        red2 = True
        while red2:
            red2 = False
            red2, poly = roots_reduction(poly, dag)
        # if red2:
        #     print("Done")
        # else:
        #     print("None")
        # print("  .. Leaves reduction .. ",end='')
        red3 = True
        while red3:
            red3 = False
            red3, poly = leaves_reduction(poly, dag)
        # if red3:
        #     print("Done")
        # else:
        #     print("None")
        # print("  .. Singles reduction .. ",end='')
        red4, poly = singles_reduction(poly, dag)
        # if red4:
        #     print("Done")
        # else:
        #     print("None")
        reduced = red4
        
    if len(dag.vertices) == 1:
        # print("  .. Alone reduction .. ",end='')
        poly = alone_reduction(poly, dag)
        # print("Done")


    rpoly = factorial(dag_len) * poly.poly
    result = str(rpoly)

    try:
        nb_linexts = int(result)
        return True, nb_linexts
    except ValueError:
        return False, result

def handle_dag(dag, outdir="dots/"):
    import os
    try:
        os.mkdir(outdir)
    except OSError:
        pass # directory exists

    print("    >>> DAG size = {}".format(len(dag)))

    #dag.to_dot_file(outdir + file_name + ".dot")

    start_time = clock()
    ok, result = count_linear_extensions(dag)
    end_time = clock()
    elapsed_time = end_time - start_time

    if ok:
        print("   [OK] ==> Count of linear extensions = {}  ({})".format(result, numerical_approx(result)))
        print("            Elapsed time: {} s".format(elapsed_time))
    else:
        print("   [KO] ==> Pending polynomial = {}".format(result))
        print("            Wasted time: {} s".format(elapsed_time))
    print("----------------------------------------")    



def make_dag1():
    dag1 = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(0)
    N0 = dag1.add_vertex()
    N1 = dag1.add_vertex()
    N2 = dag1.add_vertex()
    N3 = dag1.add_vertex()
    N4 = dag1.add_vertex()
    N5 = dag1.add_vertex()
    N6 = dag1.add_vertex()
    dag1.add_edge(N0, N1)
    dag1.add_edge(N1, N2)
    dag1.add_edge(N1, N4)
    dag1.add_edge(N2, N3)
    dag1.add_edge(N4, N5)
    dag1.add_edge(N5, N3)
    dag1.add_edge(N3, N6)

    return dag1

def make_dag2():
    dag2 = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(1)

    nodes = [dag2.add_vertex() for i in range(1,16)]
    
    def edge(i,j):
        dag2.add_edge(nodes[i-1], nodes[j-1])
        
    edge(1,2)
    edge(1,11)
    edge(2,3)
    edge(2,6)
    edge(2,7)
    edge(3,4)
    edge(3,5)
    edge(7,8)
    edge(7,9)
    edge(9,10)
    edge(11,12)
    edge(11,13)
    edge(13,14)
    edge(13,15)
    edge(11,10)
    
    return dag2

def make_dag3():
    dag3 = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(1)

    nodes = [dag3.add_vertex() for i in range(1,10)]
    
    def edge(i,j):
        dag3.add_edge(nodes[i-1], nodes[j-1])
        
    edge(1,2)
    edge(1,3)
    edge(2,4)
    edge(2,5)
    edge(3,5)
    edge(3,6)
    edge(4,7)
    edge(5,7)
    edge(5,8)
    edge(6,8)
    edge(7,9)
    edge(8,9)
    
    
    return dag3

def make_mips_dag():
    dag = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(1)
    
    nodes = [dag.add_vertex() for i in range(1,11)]
    
    def edge(i,j):
        dag.add_edge(nodes[i-1], nodes[j-1])

    edge(1, 2)
    edge(1, 3)
    edge(3, 4)
    edge(3, 5)
    edge(5, 6)
    edge(6, 7)
    edge(4, 7)
    edge(7, 8)
    edge(8, 9)
    edge(2, 10)
    edge(7, 10)
    #edge(10, 11)
    #edge(9, 11)

    return dag
        
def make_dag4():
    dag4 = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(1)

    nodes = [dag4.add_vertex() for i in range(1,19)]
    
    def edge(i,j):
        dag4.add_edge(nodes[i-1], nodes[j-1])
        
    edge(1,2)
    edge(2,3)
    edge(3,4)
    edge(4,5)

    edge(1,6)
    edge(6,7)
    edge(7,8)

    edge(8,9)

    #edge(9,10)
    #edge(10,11)
    #edge(11,12)
    #edge(12,5)

    edge(9,10)
    edge(9,11)
    edge(11,12)
    edge(10,12)
    edge(12,5)

    edge(5,13)
    edge(13,14)
    edge(14,15)
    edge(15,18)

    edge(8,16)
    edge(16,17)
    edge(17,18)

    return dag4


def make_spt_dag1(n):
    dag = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(0)

    root_node = dag.add_vertex()
    pred_node = root_node

    for k in range(1, n+1):
        next_node = dag.add_vertex()
        dag.add_edge(pred_node, next_node)
        pred_node = next_node

    middle_node = dag.add_vertex()
    dag.add_edge(root_node, middle_node)
    dag.add_edge(root_node, pred_node)
    
    node1 = dag.add_vertex()
    dag.add_edge(middle_node, node1)
    node2 = dag.add_vertex()
    dag.add_edge(node1, node2)
    end_node = dag.add_vertex()
    dag.add_edge(node2, end_node)
    dag.add_edge(pred_node, end_node)

    return dag

def make_crown_2_2_3():
    dag = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(0)

    a = dag.add_vertex()
    b = dag.add_vertex()
    c = dag.add_vertex()
    d = dag.add_vertex()
    e = dag.add_vertex()
    f = dag.add_vertex()

    ac = dag.add_vertex()
    bc = dag.add_vertex()
    abc = dag.add_vertex()

    dag.add_edge(a, ac)
    dag.add_edge(c, ac)
    dag.add_edge(c, bc)
    dag.add_edge(b, bc)
    dag.add_edge(ac, d)
    dag.add_edge(ac, abc)
    dag.add_edge(abc, e)
    dag.add_edge(bc, abc)
    dag.add_edge(bc, f)

    return dag

def make_spt_dag2(n,m):
    dag = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(0)

    root_node = dag.add_vertex()
    pred_node = root_node

    for k in range(1, n+2):
        next_node = dag.add_vertex()
        dag.add_edge(pred_node, next_node)
        pred_node = next_node

    middle_node = dag.add_vertex()
    dag.add_edge(root_node, middle_node)
    dag.add_edge(root_node, pred_node)
    
    pred_node1 = middle_node
    for k in range(1, m):
        next_node1 = dag.add_vertex()
        dag.add_edge(pred_node1, next_node1)
        pred_node1 = next_node1

    end_node = dag.add_vertex()
    dag.add_edge(pred_node1, end_node)
    dag.add_edge(pred_node, end_node)

    return dag

def make_linear(dag, start_node, end_node, nb_nodes):
    pred_node = start_node
    nodes = [start_node]
    for k in range(1, nb_nodes + 1):
        next_node = dag.add_vertex()
        nodes.append(next_node)
        dag.add_edge(pred_node, next_node)
        pred_node = next_node

    dag.add_edge(pred_node, end_node)
    nodes.append(end_node)

    return nodes

# petrinets 19 paper: 1975974
def make_pn19_dag():
    dag = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(0)

    init = dag.add_vertex()
    
    gen = dag.add_vertex()
    yield1 = dag.add_vertex()
    yield2 = dag.add_vertex()

    load = dag.add_vertex()
    xform = dag.add_vertex()
    
    step1 = dag.add_vertex()
    step2 = dag.add_vertex()
    step3 = dag.add_vertex()
    step4 = dag.add_vertex()
    end = dag.add_vertex()
    
    fork = dag.add_vertex()
    comp1 = dag.add_vertex()
    comp2_1 = dag.add_vertex()
    comp2_2 = dag.add_vertex()
    join = dag.add_vertex()

    dag.add_edge(init, gen)
    dag.add_edge(init, step1)
    dag.add_edge(init, fork)

    dag.add_edge(gen, yield1)
    dag.add_edge(yield1, yield2)
    dag.add_edge(yield1, step3)
    dag.add_edge(yield2, end)

    dag.add_edge(step1, load)
    dag.add_edge(load, xform)
    dag.add_edge(xform, step4)

    dag.add_edge(step1, step2)
    dag.add_edge(step2, step3)
    dag.add_edge(step3, step4)
    dag.add_edge(step4, end)

    dag.add_edge(fork, comp1)
    dag.add_edge(comp1, join)
    dag.add_edge(fork, comp2_1)
    dag.add_edge(comp2_1, comp2_2)
    dag.add_edge(comp2_2, join)
    dag.add_edge(join, end)

    return dag

def make_arch_dag(n, k):
    dag = daglib.DAG()
    xi =  [ dag.add_vertex() for _ in range(k)]
    ai =  [ dag.add_vertex() for _ in range(n)]
    bi =  [ dag.add_vertex() for _ in range(n)]
    ci =  [ dag.add_vertex() for _ in range(n)]

    for i in range(n):
        dag.add_edge(ai[i],ci[i])
        dag.add_edge(ci[i],bi[i])

    for i in range(n-1):
        dag.add_edge(ai[i],ai[i+1])
        dag.add_edge(bi[i],bi[i+1])

    for i in range(k-1):
        dag.add_edge(xi[i],xi[i+1])

    dag.add_edge(ai[n-1],xi[0])
    dag.add_edge(xi[k-1],bi[0])

    return dag
    
    

def make_spt_dagK(n, k):
    dag = daglib.DAG()
    
    root_node = dag.add_vertex()
    bot_node = dag.add_vertex()
    
    nodes = make_linear(dag, root_node, bot_node, k + k - 2)

    for i in range(0, k-1):
        make_linear(dag, nodes[i], nodes[i+k], n)

    return dag

def analyze_stp1_dags(nmin=3, nmax=20, increment=1):
    for n in range(nmin, nmax+1, increment):
        dag = make_spt_dag1(n)
        print(">>>>> SPT1 DAG n={} ".format(n))
        handle_dag(dag)

def analyze_stp2_dags(nmin=3, nmax=20, increment=1):
    for n in range(nmin, nmax+1, increment):
        dag = make_spt_dag2(n,n)
        print(">>>>> SPT2 DAG n={} ".format(n))
        handle_dag(dag)

def analyze_stpK_dags(n=3, kmin=1, kmax=10, kincrement=1):
    for k in range(kmin, kmax+1, kincrement):
        dag = make_spt_dagK(n,k)
        print(">>>>> SPTK DAG k={} n={} ".format(k, n))
        handle_dag(dag)

if __name__ == "__main__":
    dag1 = make_dag1()
    print(count_linear_extensions(dag1))

    # dag2 = make_dag2()
    # count_linear_extensions(dag2)

    # dag3 = make_dag3()
    # print(count_linear_extensions(dag3))


    # dag4 = make_dag4()
    # dag4.to_dot_file("dag4.dot")
    # print(count_linear_extensions(dag4))

    #mips_dag = make_mips_dag()
    #print(count_linear_extensions(mips_dag))

    #crown_dag = make_crown_2_2_3()
    #print(count_linear_extensions(crown_dag))

    # pn19_dag = make_pn19_dag()
    # print(count_linear_extensions(pn19_dag))

    #arch_dag = make_arch_dag(10,1)
    #print(count_linear_extensions(arch_dag))
