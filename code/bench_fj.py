#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
from time import time
import os, sys, signal, random
from decimal import Decimal

import daglib
from ImportTaskDag import import_task_dag

import RandLinExtSage as BIT
import random_gen_arch_generalise_v2 as Arch
import Huber, ForkJoin

from benchlib import *

sys.setrecursionlimit(10**6)

# random.seed(hash("ish"))
random.seed(hash("tag bench"))
sizes = [] #[10,30,100]
timeout = 100
nb_dags = 10

# Times for dags of size in sizes
for size in sizes:

    dags = [ForkJoin.randomFJ(size) for _ in range(nb_dags)]
    
    bench_fj = [BenchFJ(fj) for fj in dags]

    dags = [fj.to_dag() for dag in dags]
    avg_size = reduce(lambda a, b: a+b, map(len, dags))/nb_dags

    bench_huber = [BenchHuber(dag) for dag in dags]
    bench_bit = [BenchBIT(dag) for dag in dags]

    counting_fj = CountingRunner(bench_fj, timeout)
    sampling_fj = SamplingRunner(bench_fj, timeout)

    counting_bit = (0,0,0,0,0,0)
    sampling_bit = (0,0,0,0,0)
    sampling_huber = (0,0,0,0,0)
    
    if size < 50 :
        counting_bit, sampling_bit = WholeRunner(bench_bit, timeout)
        sampling_huber = SamplingRunner(bench_huber, timeout)

    print("""           ------ Counting FJ ------
#graphs : {}, average size : {}, average #le : {}

algo, #handled, tmin, tmed, tmax, tavg
FJ, {}, {}, {}, {}, {}
BIT, {}, {}, {}, {}, {}
""".format(nb_dags, avg_size, counting_fj[0], *(counting_fj[1:] + counting_bit[1:])))

    print("""           ------ Sampling FJ ------
#graphs : {}, average size : {}, average #le : {}

algo, #handled, tmin, tmed, tmax, tavg
FJ, {}, {}, {}, {}, {}
BIT, {}, {}, {}, {}, {}
Huber, {}, {}, {}, {}, {}
""".format(nb_dags, avg_size, counting_fj[0], *(sampling_fj + sampling_bit + sampling_huber)))


# Biggest DAG handled for BIT and Huber

dags = [ForkJoin.randomFJ(size) for size in range(30,201,10)]

limit_bit = (0,0,0,0,0,0,0)
limit_huber = (0,0,0,0,0)
stop_bit = False
stop_huber = False


for dag in dags :

    if stop_bit and stop_huber:
        break

    bfj = BenchFJ(dag)

    dag = dag.to_dag()
    dag_size = len(dag)

    bhuber = BenchHuber(dag)
    bbit = BenchBIT(dag)
    
    if not stop_bit:
        resc = CountingRunner([bbit], timeout)
        
        if resc[0] != 0:
            res = SamplingRunner([bbit], timeout)
        else:
            stop_bit = True
            
        if not stop_bit and res[-1] != 0:
            htime = SamplingRunner([bhuber], timeout)
            fjtime = SamplingRunner([bfj], timeout, 3)
            cfjtime = CountingRunner([bfj], timeout)
            limit_bit = (dag_size, Decimal(cfjtime[0]), resc[-1], res[-1], cfjtime[-1], htime[-1], fjtime[-1])
        else:
            stop_bit = True
            
    if not stop_huber:
        res = SamplingRunner([bhuber], timeout)
        
        if not stop_huber and res[-1] != 0:
            fjtime = SamplingRunner([bfj], timeout, 3)
            cfjtime = CountingRunner([bfj], timeout)
            limit_huber = (dag_size, Decimal(cfjtime[0]), res[-1], cfjtime[-1], fjtime[-1])
        else:
            stop_huber = True
    
print("""           ------ Biggest FJ ------

algo, size, #le, BIT, Huber, FJ counting, FJ sampling
Bit counting, {}, {:.4g}, {:.3g}, {:.3g}, {:.3g}, {:.3g}
BIT sampling,  {}, {:.4g}, {:.3g}
Huber, {}, {:.4g}, -, {:.3g}, {:.3g}, {:.3g}""".format(
    limit_bit[0], limit_bit[1], limit_bit[2], limit_bit[5], limit_bit[4], limit_bit[6],
    limit_bit[0], limit_bit[1], limit_bit[3],
    *limit_huber))


for size in range(100000, 1000001, 100000):
    fj = ForkJoin.randomFJ(size)
    bfj = BenchFJ(fj)
    s = SamplingRunner([bfj],timeout)
    if s[-1] >= 5:
        c = CountingRunner([bfj],0)
        print("""FJ, {}, {:.4g}, -, -, {:.3g}, {:.3g}""".format(len(fj), Decimal(c[0]), c[-1], s[-1]))
        break
