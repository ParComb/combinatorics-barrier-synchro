
(**

  * The calculus of barrier synchronization processes

 *)

Require Import Bool.

Section csb.


(**

  ** Syntax

We define the syntax of the calculus of synchronization barriers. 
It is a very simple process algebra as far as the syntax is concerned.

  *** Barriers

The type of barriers is left mostly abstract.
We however require that equality is decidable for barriers.

*)

Variable Barrier : Set.
Hypothesis Barrier_eq_dec : forall b b' : Barrier, {b = b'} + {b <> b'}.

(**

We also define a computational equality for barriers.

*)

Definition barr_eq (b b' : Barrier) : bool :=
  match (Barrier_eq_dec b b') with
    | left _ => true
    | right _ => false
  end.

(**

We need to relate the computational equality to the logical one.

*)

Proposition barr_eq_eq_l:
  forall b b' : Barrier,
  (barr_eq b b') = true -> b = b'.
Proof.
  intros b b' H.
  unfold barr_eq in H.
  destruct (Barrier_eq_dec b b') as [Heq | Hneq].
  - (* b = b' *)
    exact Heq.
  - (* b <> b' *)
    inversion H.
Qed.


Proposition barr_eq_eq_r:
  forall b b' : Barrier,
    b = b' -> (barr_eq b b') = true.
Proof.
  intros b b' H.
  rewrite H.
  unfold barr_eq.
  destruct (Barrier_eq_dec b' b') as [Heq | Hneq].
  - reflexivity.
  - contradict Hneq. reflexivity.
Qed.

(**

This gives us a simple way to prove basic computational equality results on barriers.

*)

Proposition barr_eq_refl:
  forall b : Barrier,
    barr_eq b b = true.
Proof.
  intro b.
  apply barr_eq_eq_r.
  reflexivity.
Qed.

(**

Of course this also works for inequality.

*)

Proposition barr_neq_neq_l:
  forall b b' : Barrier,
  (barr_eq b b') = false -> b <> b'.
Proof.
  intros b b' H.
  intro Heq.
  rewrite <- Heq in H.
  rewrite barr_eq_refl in H.
  inversion H.
Qed.  

Proposition barr_neq_neq_r:
  forall b b' : Barrier,
    b <> b' -> (barr_eq b b') = false.
Proof.
  intros b b' H.
  case_eq (barr_eq b b').
  - intro Htrue.
    apply barr_eq_eq_l in Htrue.
    contradiction.
  - intro Hfalse.
    reflexivity.
Qed.

(**

  *** Actions

The set of actions is also left abstract with decidable equality. Unlike many process
algebras which rely on actions for concurrency control, such as the action names in
 the case of CCS, barrier synchronization constructs are kept separate from the other actions
performed by the processes. 

*)

Variable Action : Set.
Hypothesis Action_eq_dec : forall a a' : Action, {a = a'} + {a <> a'}.

(**

  *** Processes

The syntactic set of CSB processes is defined inductively as follows.

*)

Inductive Process : Set :=
| proc_term: Process
| proc_act: Action -> Process -> Process
| proc_new: Barrier -> Process -> Process
| proc_sync: Barrier -> Process -> Process
| proc_par: Process -> Process -> Process.


(* FIXME : notations do not really work as expected ...
Notation "0" := proc_term.
Definition δ := proc_dead.
Notation "a , p" := (proc_act a p) (at level 45, right associativity).
Notation "'new' ( b ) p" := (proc_new b p) (at level 75, right associativity).

Section notation_ex1.
Variable B : Barrier.
Variable a b c : Action.
Check (a , 0).
Check a, b, 0.
Check new(B) (a, b, 0).
End notation_ex1.

Notation "<< b >> p" := (proc_sync b p) (at level 75, right associativity).
Notation "p || q" := (proc_par p q).

Section notation_ex2.
Variable B : Barrier.
Variable a b c : Action.
Check a, 0.
Check a, b, 0.
Check <<B>> (b, 0).
Check new(B) (a, b, 0) || (a, <<B>> (b, 0)) || <<B>> (c, 0).
End notation_ex2.

*)

(**

  ** Semantics

We adopt the standard framework of labelled transition systems to formalize the
operational semantics of CSB processes.

*)

(**

  *** Waiting synchronizations

[Waiting b p] says that process [p] knows the barrier [b] and
require at some point to synchronize with it in one of its subprocesses.

The relation [Waiting] is defined inductively as follows.

*)

Inductive Waiting : Barrier -> Process -> Prop :=
| wait_act: forall b : Barrier, forall p : Process, forall a : Action,
        Waiting b p
        -> Waiting b (proc_act a p)
| wait_new: forall b b' : Barrier, forall p : Process, 
      Waiting b p -> b <> b'
      -> Waiting b (proc_new b' p)
| wait_sync_eq: forall b : Barrier, forall p : Process,
      Waiting b (proc_sync b p)
| wait_sync_neq: forall b b' : Barrier, forall p : Process,
      Waiting b p -> b <> b'
      -> Waiting b (proc_sync b' p)
| wait_lpar: forall b : Barrier, forall p q : Process,
      Waiting b p
      -> Waiting b (proc_par p q)
| wait_rpar: forall b : Barrier, forall p q : Process,
      Waiting b q
      -> Waiting b (proc_par p q).

(**

The relation can be computed as a functional predicate,
 which gives us a computational definition.

*)

Fixpoint is_waiting (b:Barrier) (p:Process) : bool :=
  match p with
    | proc_term => false
    | proc_act a p' => is_waiting b p'
    | proc_new b' p' => if (barr_eq b b') then false
                        else (is_waiting b p')
    | proc_sync b' p' => if (barr_eq b b') then true
                         else is_waiting b p'
    | proc_par p' q' => orb (is_waiting b p')
                            (is_waiting b q')
  end.

Lemma Waiting_is_waiting:
  forall b : Barrier, forall p : Process,
    Waiting b p -> is_waiting b p = true.
Proof.
  intros b p H.
  induction H.
  - simpl.
    exact IHWaiting.
  - simpl.
    case_eq (barr_eq b b').
    + intro Htrue.
      apply barr_eq_eq_l in Htrue.
      contradiction.
    + intro Hfalse.
      exact IHWaiting.
  - simpl.
    rewrite barr_eq_refl.
    reflexivity.
  - simpl.
    case_eq (barr_eq b b').
    + intro Htrue.
      reflexivity.
    + intro Hfalse.
      exact IHWaiting.
  - simpl.
    rewrite IHWaiting.
    (* SearchPattern ( _ || _ = true ).
       orb_true_l: forall b : bool, true || b = true
     *)
    apply orb_true_l.
  - simpl.
   rewrite IHWaiting.
    (* SearchPattern ( _ || _ = true ).
       orb_true_r: forall b : bool, b || true = true
     *)
    apply orb_true_r.
Qed. 


Lemma is_waiting_Waiting:
  forall b : Barrier, forall p : Process,
    is_waiting b p = true -> Waiting b p.
Proof.
  intros b p H.
  induction p.
  - (* case proc_term *)
    simpl in H.
    inversion H.
  - (* case proc_act *)
    apply wait_act.
    apply IHp.
    simpl in H.
    exact H.
  - (* case proc_new *)
    destruct (Barrier_eq_dec b b0) as [Heq | Hneq].
    + (* b=b0 *)
      rewrite <- Heq in H.
      simpl in H.
      rewrite barr_eq_refl in H.
      inversion H.
    + (* b<>b0 *)
      apply wait_new.
      * { simpl in H.
          rewrite barr_neq_neq_r in H.
          - apply IHp.
            exact H.
          - exact Hneq.
        }
      * exact Hneq.
  - (* case proc_sync *)
    destruct (Barrier_eq_dec b b0) as [Heq | Hneq].
    + (* b=b0 *)
      rewrite <- Heq.
      apply wait_sync_eq.
    + (* b<>b0 *)
      apply wait_sync_neq.
      * { apply IHp.
          simpl in H.
          rewrite barr_neq_neq_r in H.
          - exact H.
          - exact Hneq.
        }
      * exact Hneq.
  - (* case proc_par *)        
    simpl in H.
    (* SearchPattern ( _ || _ = true -> _ ).
       orb_prop: forall a b : bool, a || b = true -> a = true \/ b = true *)
    apply orb_prop in H.
    destruct H as [Hleft | Hright].
    + apply wait_lpar.
      apply IHp1.
      exact Hleft.
    + apply wait_rpar.
      apply IHp2.
      exact Hright.
Qed.

(**

  *** Synchronizations

[Sync b p q] expresses that process [p] becomes [q] after synchronization
 on barrier [b]. There are then three cases to consider :

 - if [q = p] then no synchronization took place
 - if [Waiting b q] then the synchronization is incomplete, in that there is still
   a subprocess of [q] which has [b] waiting
 - otherwise, the synchronization has been performed correctly. 

Note that in practice the first case can be considered as a complete synchronization since
no one was waiting on the barrier and thus there is nothing to do to synchronize.

This synchronization principle can be defined inductively as follows.


*)

Inductive Sync : Barrier -> Process -> Process -> Prop :=
| sync_term: forall b : Barrier, Sync b proc_term proc_term
| sync_act: forall b : Barrier, forall a : Action, forall p : Process,
             Sync b (proc_act a p) (proc_act a p)
| sync_new_eq: forall b : Barrier, forall p : Process,
    Sync b (proc_new b p) (proc_new b p)
| sync_new_neq: forall b b': Barrier, forall p p' : Process,
                  Sync b p p' -> b <> b'
                  -> Sync b (proc_new b' p) (proc_new b' p')
| sync_sync_eq: forall b : Barrier, forall p : Process,
                  Sync b (proc_sync b p) p
| sync_sync_neq: forall b b' : Barrier, forall p : Process,
                   b <> b'
                   -> Sync b (proc_sync b' p) (proc_sync b' p)
| sync_par: forall b : Barrier, forall p p' q q' : Process,
              Sync b p p' -> Sync b q q'
              -> Sync b (proc_par p q) (proc_par p' q').


(**

To separate the two possible outcomes we provide the following definition.

 *)

Definition CompleteSync (b : Barrier) (p : Process) (q : Process) : Prop :=
  Sync b p q  /\  ~(Waiting b q).

Definition IncompleteSync (b : Barrier) (p : Process) (q : Process) : Prop :=
  Sync b p q /\ Waiting b q.

(*

Once again, we can describe quite a similar principle in a functional way,
 as follows.

 *)

Fixpoint synchronize (b:Barrier) (p:Process) : Process :=
  match p with
    | proc_term => p
    | proc_act a p' => p
    | proc_new b' p' => if (barr_eq b b') then p 
                        else proc_new b' (synchronize b p')
    | proc_sync b' p' => if (barr_eq b b') then p'
                         else p (* blocked *)
    | proc_par p' q' => proc_par (synchronize b p')
                                 (synchronize b q')
  end.

(**

And now the correspondance lemmas.

 *)

Lemma Sync_synchronize:
  forall b : Barrier, forall p q : Process,
    Sync b p q -> synchronize b p = q.
Proof.
  intros b p q H.
  induction H.
  - (* case sync_term *)
    simpl.
    reflexivity.
  - (* case sync_act *)
    simpl.
    reflexivity.
  - (* case sync_new_eq *)
    simpl.
    rewrite barr_eq_refl.
    reflexivity.
  - (* case sync_neq_neq *)
    simpl.
    rewrite IHSync.
    rewrite barr_neq_neq_r.
    + reflexivity.
    + exact H0.
  - (* case sync_sync_eq *)
    simpl.
    rewrite barr_eq_refl.
    reflexivity.
  - (* case sync_sync_neq *)
    simpl.
    rewrite barr_neq_neq_r.
    + reflexivity.
    + exact H.
  - (* case sync_par *)
    simpl.
    rewrite IHSync1.
    rewrite IHSync2.
    reflexivity.
Qed.

Lemma synchronize_Sync:
  forall b : Barrier, forall p q : Process,
    synchronize b p = q -> Sync b p q.
Proof.
  induction p.
  - (* case proc_term *)
    intros q H.
    simpl in H.
    rewrite <- H.
    apply sync_term.
  - (* case proc_act *)
    intros q H.
    simpl in H.
    rewrite <- H.
    apply sync_act.
  - (* case proc_new *)
    intros q H.
    simpl in H.
    destruct (Barrier_eq_dec b b0) as [Heq | Hneq].
    + (* b=b0 *)
      rewrite <- Heq in *.
      rewrite barr_eq_refl in H.
      rewrite <- H.
      apply sync_new_eq.
    + (* b<>b0 *)
      rewrite barr_neq_neq_r in H.
      * { rewrite <- H.
          apply sync_new_neq.
          - apply IHp.
            reflexivity.
          - exact Hneq.
        }
      * exact Hneq.
  - (* case proc_sync *)
    intros q H.
    simpl in H.
    destruct (Barrier_eq_dec b b0) as [Heq | Hneq].
    + (* b=b0 *)
      rewrite <- Heq in *.
      rewrite barr_eq_refl in H.
      rewrite <- H.
      apply sync_sync_eq.
    + (* b<>b0 *)
      rewrite barr_neq_neq_r in H.
      * { rewrite <- H.
          apply sync_sync_neq.
          exact Hneq.
        }
      * exact Hneq.
  - (* case proc_par *)
    intros q H.
    simpl in H.
    rewrite <- H.
    apply sync_par.
    + (* case lpar *)
      apply IHp1.
      reflexivity.
    + (* case rpar *)
      apply IHp2.
      reflexivity.
Qed.


(**

  *** Transitions 

We now formalize the core of the semantics, by the means of single-step labelled transitions.

The labels are either atomic actions or termination markers.

*)

Inductive Label : Set :=
| lbl_atomic: Action -> Label.

(**

The transition relation is defined inductively.

[Transition p alpha p'] means the process [p] can evolve to process [p'] for an
atomic action or a termination event [alpha].

*)

Inductive Transition : Process -> Label -> Process -> Prop :=
| sem_act: forall a:Action, forall p:Process,
             Transition (proc_act a p) (lbl_atomic a) p 
| sem_new_lift: forall b:Barrier, forall p q p':Process, forall alpha:Label,
                  Sync b p q -> Waiting b q  (* incomplete synchro *)
                  -> Transition p alpha p'
                  -> Transition (proc_new b p) alpha (proc_new b p')
| sem_new_sync: forall b:Barrier, forall p q q':Process, forall alpha:Label,
                  Sync b p q -> ~(Waiting b q) (* complete synchro *)
                  -> Transition q alpha q'
                  -> Transition (proc_new b p) alpha q'
| sem_lpar: forall p p' q:Process, forall alpha:Label,
              Transition p alpha p'
              -> Transition (proc_par p q) alpha (proc_par p' q)
| sem_rpar: forall p q q':Process, forall alpha:Label,
              Transition q alpha q'
              -> Transition (proc_par p q) alpha (proc_par p q').

Section example1.
  
Variable B : Barrier.
Variable fork : Action.
Variable start1 end1 : Action.
Variable start2 end2 : Action.
Variable start3 end3 : Action.

Definition P:Process :=
  proc_new B 
           (proc_act fork 
                     (proc_par (proc_act start1 (proc_act end1 (proc_sync B proc_term)))
                               (proc_par (proc_act start2 (proc_sync B (proc_act end2 proc_term)))
                                         (proc_sync B (proc_act start3 (proc_act end3 proc_term)))))).

Definition P':Process :=
  (proc_new B
            (proc_par (proc_act start1 (proc_act end1 (proc_sync B proc_term)))
                      (proc_par (proc_act start2 (proc_sync B (proc_act end2 proc_term)))
                                (proc_sync B (proc_act start3 (proc_act end3 proc_term)))))).

Example trans1:
  Transition P (lbl_atomic fork) P'.
Proof.
  apply sem_new_lift with (q:=(proc_act fork 
                     (proc_par (proc_act start1 (proc_act end1 (proc_sync B proc_term)))
                               (proc_par (proc_act start2 (proc_sync B (proc_act end2 proc_term)))
                                         (proc_sync B (proc_act start3 (proc_act end3 proc_term))))))).
  + apply sync_act.
  + apply wait_act.
    apply wait_lpar.
    apply wait_act.
    apply wait_act.
    apply wait_sync_eq.
  + apply sem_act.
Qed.
    
Definition Q1:Process :=
  (proc_new B
            (proc_par (proc_act end1 (proc_sync B proc_term))
                      (proc_par (proc_act start2 (proc_sync B (proc_act end2 proc_term)))
                                (proc_sync B (proc_act start3 (proc_act end3 proc_term)))))).

Example trans2:
  Transition P' (lbl_atomic start1) Q1.
Proof.
  apply sem_new_lift with (q:=(proc_par (proc_act start1 (proc_act end1 (proc_sync B proc_term)))
                               (proc_par (proc_act start2 (proc_sync B (proc_act end2 proc_term)))
                                         (proc_act start3 (proc_act end3 proc_term))))).
  + apply sync_par.
    - apply sync_act.
    - apply sync_par.
      * apply sync_act.
      * apply sync_sync_eq.
  + apply wait_lpar.
    apply wait_act.
    apply wait_act.
    apply wait_sync_eq.
  + apply sem_lpar.
    apply sem_act.
Qed.

Definition Q2:Process :=
  (proc_new B
            (proc_par (proc_act start1 (proc_act end1 (proc_sync B proc_term)))
                      (proc_par (proc_sync B (proc_act end2 proc_term))
                                (proc_sync B (proc_act start3 (proc_act end3 proc_term)))))).

Example trans2':
  Transition P' (lbl_atomic start2) Q2.
Proof.
  apply sem_new_lift with (q:=(proc_par (proc_act start1 (proc_act end1 (proc_sync B proc_term)))
                                        (proc_par (proc_act start2 (proc_sync B (proc_act end2 proc_term)))
                                                  (proc_act start3 (proc_act end3 proc_term))))).
  + apply sync_par.
    - apply sync_act.
    - apply sync_par.
      * apply sync_act.
      * apply sync_sync_eq.
  + apply wait_lpar.
    apply wait_act.
    apply wait_act.
    apply wait_sync_eq.
  + apply sem_rpar.
    apply sem_lpar.
    apply sem_act.
Qed.    

Example ntrans2'':
  forall act : Action,
    act <> start1 -> act <> start2 
    -> ~ (exists Q3:Process, Transition P' (lbl_atomic act) Q3).
Proof.  
  intros act Hneq1 Hneq2.
  unfold not.
  intro Hex.
  destruct Hex as [P'' H].
  inversion_clear H.
  - clear H0 H1 q. 
    inversion_clear H2.
    + inversion H.
      rewrite H3 in Hneq1.
      contradict Hneq1. reflexivity.
    + inversion_clear H.
      * inversion H0.
        rewrite H in Hneq2.
        contradict Hneq2. reflexivity.
      * inversion_clear H0.
  -  apply Sync_synchronize in H0.
     simpl in H0.
     rewrite barr_eq_refl in H0.
     rewrite <- H0 in H1.
     unfold not in H1.
     apply H1.
     apply is_waiting_Waiting.
     simpl.
     rewrite barr_eq_refl.
     trivial.
Qed.

Definition Q4:Process :=
  (proc_new B
            (proc_par (proc_act end1 (proc_sync B proc_term))
                      (proc_par (proc_sync B (proc_act end2 proc_term))
                                (proc_sync B (proc_act start3 (proc_act end3 proc_term)))))).

Example trans3:
  Transition Q1 (lbl_atomic start2) Q4.
Proof.
  apply sem_new_lift with (q:=(proc_par (proc_act end1 (proc_sync B proc_term))
                                        (proc_par (proc_act start2 (proc_sync B (proc_act end2 proc_term)))
                                                  (proc_act start3 (proc_act end3 proc_term))))).
  - apply sync_par.
    + apply sync_act. 
    + apply sync_par.
      * apply sync_act.
      * apply sync_sync_eq.
  - apply wait_lpar.
    apply wait_act.
    apply wait_sync_eq.
  - apply sem_rpar.
    apply sem_lpar.
    apply sem_act.
Qed.    

Definition Q5:Process :=
  (proc_new B
            (proc_par (proc_sync B proc_term)
                      (proc_par (proc_sync B (proc_act end2 proc_term))
                                (proc_sync B (proc_act start3 (proc_act end3 proc_term)))))).

Example trans4:
  Transition Q4 (lbl_atomic end1) Q5.
Proof.
  apply sem_new_lift with (q:=(proc_par (proc_act end1 (proc_sync B proc_term))
                                        (proc_par (proc_act end2 proc_term)
                                                  (proc_act start3 (proc_act end3 proc_term))))).
  - apply sync_par.
    + apply sync_act.
    + apply sync_par.
      * apply sync_sync_eq.
      * apply sync_sync_eq. 
  - apply wait_lpar.
    apply wait_act.
    apply wait_sync_eq.
  - apply sem_lpar.
    apply sem_act.
Qed.

Example ntrans4:
  forall act : Action,
    act <> end1
    -> ~ (exists Q4':Process, Transition Q4 (lbl_atomic act) Q4').
Proof.
  intros act Hneq Hex.
  destruct Hex as [Q4' H].
  inversion_clear H.
  - inversion_clear H2.
    + inversion H.
      rewrite H5 in Hneq.
      contradict Hneq. reflexivity.
    +  inversion_clear H.
       * inversion H2.
       * inversion H2.
  - apply Sync_synchronize in H0.
    simpl in H0.
    rewrite barr_eq_refl in H0.
    apply H1.
    rewrite <- H0.
    apply is_waiting_Waiting.
    simpl.
    rewrite barr_eq_refl.
    trivial.
Qed.    


Definition Q6:Process :=
  (proc_par proc_term
            (proc_par proc_term
                      (proc_act start3 (proc_act end3 proc_term)))).

Example trans5:
  Transition Q5 (lbl_atomic end2) Q6.
Proof.
  apply sem_new_sync with (q:=(proc_par proc_term
                                        (proc_par (proc_act end2 proc_term)
                                                  (proc_act start3 (proc_act end3 proc_term))))).
  - apply sync_par.
    + apply sync_sync_eq.
    + apply sync_par.
      * apply sync_sync_eq.
      * apply sync_sync_eq.
  - intro H.
    inversion_clear H.
    + inversion H0.
    + inversion_clear H0.
      * inversion_clear H.
        inversion H0.
      * inversion_clear H.
        inversion_clear H0.
        inversion H.
  - apply sem_rpar.
    apply sem_lpar.
    apply sem_act.
Qed.

End example1.

Inductive Terminated: Process -> Prop :=
| term_term: Terminated proc_term
| term_new: forall b:Barrier, forall p:Process,
              Terminated p
              -> Terminated (proc_new b p)
| term_par: forall p q:Process,
              Terminated p -> Terminated q
              -> Terminated (proc_par p q).

Fixpoint is_terminated (p:Process) : bool :=
  match p with
    | proc_term => true
    | proc_act a p' => false
    | proc_new b p' => is_terminated p'
    | proc_sync b p' => false
    | proc_par p' q' => (is_terminated p') && (is_terminated q')
  end.

Lemma Terminated_is_terminated:
  forall p : Process,
    Terminated p -> is_terminated p = true.
Proof.
  intros p H.
  induction H.
  - simpl.
    reflexivity.
  - simpl.
    exact IHTerminated.
  - simpl.
    rewrite IHTerminated1.
    rewrite IHTerminated2.
    simpl.
    reflexivity.
Qed.    

Lemma is_terminated_Terminated:
  forall p : Process,
    is_terminated p = true -> Terminated p.
Proof.
  induction p.
  - (* proc_term *)
    intro H.
    apply term_term.
  - (* proc_act *)
    intro H.
    simpl in H.
    inversion H.
  - (* proc_new *)
    intro H.
    apply term_new.
    apply IHp.
    simpl in H.
    exact H.
  - (* proc_sync *)
    intro H.
    simpl in H.
    inversion H.
  - (* proc_par *)
    intro H.
    simpl in H.
    (* SearchPattern (_ && _ = true -> _).
       andb_prop: forall a b : bool, a && b = true -> a = true /\ b = true *)
    apply andb_prop in H.
    destruct H as [H1 H2].
    apply term_par.
    + (* left *)
      apply IHp1.
      exact H1.
    + (* right *)
      apply IHp2.
      exact H2.
Qed.

Definition Dead (p:Process) : Prop :=
  ~(Terminated p)
  /\ forall q:Process, forall alpha:Label,
       ~(Transition p alpha q).


Inductive ExtLabel : Set :=
| elbl_atomic: Action -> ExtLabel
| elbl_sig: Barrier -> ExtLabel
| elbl_sync: Barrier -> ExtLabel.

Inductive ExtTransition : Process -> ExtLabel -> Process -> Prop :=
| sem_eact: forall a:Action, forall p:Process,
             ExtTransition (proc_act a p) (elbl_atomic a) p 
| sem_esig: forall b:Barrier, forall p:Process,
             ExtTransition (proc_sync b p) (elbl_sig b) p
| sem_enew_lift: forall b:Barrier, forall p p':Process, forall mu:ExtLabel,
        ExtTransition p mu p'
        -> mu <> elbl_sig b
        -> ExtTransition (proc_new b p) mu (proc_new b p')
| sem_enew_sync: forall b:Barrier, forall p p' p'':Process,
      ExtTransition p (elbl_sig b) p'
      -> ExtTransition (proc_new b p) (elbl_sync b) p'
| sem_elpar_sync: forall p p' q q':Process, forall b : Barrier, forall mu:ExtLabel,
        ExtTransition p (elbl_sig b) p'
        -> ExtTransition q mu q'
        -> mu <> elbl_sig b
      -> ExtTransition (proc_par p q) (elbl_sig b) (proc_par p' q).





End csb.

