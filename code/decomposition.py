import utils, os

from daglib import *
    
nb_cf_dags = 0
    
def roots_reduction(dag):
    reduced = False
    roots = compute_roots_of_vertices(dag.vertices)

    for root in roots:
        if len(root.nexts) == 1:
            #print("[REDUCTION] remove root N{}".format(root.id))
            dag.remove_vertex(root)
            reduced = True

    return reduced

def leaves_reduction(dag):
    reduced = False
    leaves = compute_leaves_of_vertices(dag.vertices)

    for leaf in leaves:
        if len(leaf.preds) == 1:
            #print("[REDUCTION] remove leaf N{}".format(leaf.id))
            dag.remove_vertex(leaf)
            reduced = True

    return reduced

def single_vertex_reduction(dag):
    reduced = False
    for vertex in dag.vertices.copy():
        if len(vertex.preds)==1 and len(vertex.nexts)==1:
            #print("[REDUCTION] remove single N{}".format(vertex.id))
            
            dag.add_edge(utils.set_take_one(vertex.preds), utils.set_take_one(vertex.nexts))
            dag.remove_vertex(vertex)
            reduced = True
            
    return reduced


def decompose_dag(dag):
    start_size = len(dag.vertices)
    
    continue_reduction = True
    while continue_reduction:
        trans_reduced = False
        trans_reduced = dag.transitive_reduction()
        root_reduced = False
        # root_reduced = roots_reduction(dag)
        leaf_reduced = False
        leaf_reduced = leaves_reduction(dag)
        single_reduced = single_vertex_reduction(dag)

        if (not trans_reduced) and (not root_reduced) and (not leaf_reduced) and (not single_reduced):
            continue_reduction = False

    return start_size, len(dag.vertices)

def handle_task_dag(dag, counter, prefix="dag", outdir="dots/", report=True):

    global nb_cf_dags
    
    start_size, end_size = decompose_dag(CopyDAG(dag))

    if report:
        if end_size > 1:
            # dag.to_dot_file(outdir + prefix + "-reduced.dot")
            os.remove("task_dags/"+prefix)
        else:
            nb_cf_dags += 1


            
        print("DAG #{0}: {1}".format(counter, prefix))    
        print("==> size={0} reduced to {1} vertices".format(start_size, end_size))
        print("----------------------------------------")    

    return start_size, end_size

def handle_task_file(dir_name, file_name, counter, outdir="dots/", report=True):
    import os
    try:
        os.mkdir(outdir)
    except OSError:
        pass # directory exists

    import ImportTaskDag as task
    dag = DAG()
    task.import_task_dag(dir_name + file_name, dag)

    if report:
        dag.to_dot_file(outdir + file_name + ".dot")

    return handle_task_dag(dag, counter, file_name, outdir, report)

def decompose_all_task_dags():
    print("----------------------------------------") 
    import os
    count = 1
    for fname in os.listdir("./task_dags"):
        if fname.startswith("n"):
            DAGVertex.NODE_ID = 1
            handle_task_file("task_dags/", fname, count, report=True)
            count += 1

    
if __name__ == "__main__":

    decompose_all_task_dags()
    print("nb_cf_dags : {}".format(nb_cf_dags))


    
