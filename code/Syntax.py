#!/usr/bin/env python
# coding: utf-8

# In[57]:


import dot_magic

import copy


# In[58]:


from IPython.display import display, Math, Latex


# # The syntax and semantics of barrier synchronization processes

# $$  \begin{array}{lll}
# P,Q \text{::=} & 0 & \text{(termination)}\\
#       & \text{| } \alpha.P & \text{(atomic action)} \\
#       & \text{| }  \nu(B)P & \text{(barrier)} \\
#       & \text{| }  \langle B \rangle P & \text{(synchronization)} \\
#       & \text{| }  P \parallel Q & \text{(parallel)}  \\
#   \end{array}
# $$

# In[86]:


class Process:
    def __init__(self):
        pass
    
class Term(Process):
    def __init__(self):
        pass
    
    def to_latex(self):
        return "0"
    
    def __str__(self):
        return "0"
    
class Prefix(Process):
    def __init__(self, act, cont):
        self.act = act
        self.cont = cont
        
    def to_latex(self):
        latex = "{}".format(self.act)
        if isinstance(self.cont, Term):
            return latex
        else:
            return latex + "." + self.cont.to_latex()
        
    def __str__(self):
        return "{}.{}".format(self.act, self.cont)
    
class Barrier(Process):
    def __init__(self, barrier, proc):
        self.barrier = barrier
        self.proc = proc
        
    def to_latex(self):
        return "\\nu({})[{}]".format(self.barrier, self.proc.to_latex())
        
    def __str__(self):
        return "nu({})[{}]".format(self.barrier, self.proc)
    
class Synchro(Process):
    def __init__(self, barrier, proc):
        self.barrier = barrier
        self.proc = proc
        
    def __str__(self):
        return "<{}>{}".format(self.barrier, self.proc)
    
    def to_latex(self):
        return "\\langle{{{}}}\\rangle {}".format(self.barrier, self.proc.to_latex())
    
class Parallel(Process):
    def __init__(self, left, right):
        self.left = left
        self.right = right
        
    def __str__(self):
        ret = "("
        #if not isinstance(self.left, Parallel):
        #    ret += "("
        ret += str(self.left)
        ret += " || "
        ret += str(self.right)
        #if not isinstance(self.right, Parallel):
        ret += ")"
        return ret
    
    def to_latex(self):
        ret = "("
        #if not isinstance(self.left, Parallel):
        #    ret += "("
        ret += self.left.to_latex()
        ret += " \\parallel "
        ret += self.right.to_latex()
        #if not isinstance(self.right, Parallel):
        ret += ")"
        return ret
            
def par(*procs):
    parproc = procs[0]
    
    for proc in procs[1:]:
        parproc = Parallel(parproc, proc)
        
    return parproc
    


# In[87]:


proc1 = Barrier('B', Prefix('fork', 
            par(Prefix('start_1', Prefix('end_1', Synchro('B', Term()))),
                Prefix('start_2', Synchro('B', Prefix('end_2', Term()))),
                Synchro('B', Prefix('start_3', Prefix('end_3', Term()))))))


# In[88]:


#print(proc1)


# In[89]:


#display(Math(proc1.to_latex()))


# In[90]:


proc2 = Barrier('A', Barrier('B'
        ,par(Prefix('a1', Synchro('A', Prefix('a2', Term())))
             ,Synchro('B', Prefix('b', Term()))
             ,Synchro('B', Synchro('A', Prefix('c', Term()))))))


# In[91]:


#print(proc2)


# ## Synchronizing

# $\text{sync}_B(0) = 0$

# In[92]:


def _term_sync(term, barrier):
    return term

Term.sync = _term_sync


# $\text{sync}_B(\alpha.P) = \alpha.P$

# In[93]:


def _prefix_sync(prefix, barrier):
    return prefix

Prefix.sync = _prefix_sync


# $\text{sync}_B(P \parallel Q) = \text{sync}_B(P) \parallel \text{sync}_B(Q)$

# In[10]:


def _parallel_sync(par_proc, barrier):
    return Parallel(par_proc.left.sync(barrier),
                    par_proc.right.sync(barrier))

Parallel.sync = _parallel_sync


#   - $\text{sync}_B((\nu B) P) = (\nu B) P$
#   
#   - $\forall C\neq B,~\text{sync}_B((\nu C) P) = (\nu C) \text{sync}_B(P)$

# In[11]:


def _barrier_sync(bar_proc, barrier):
    if bar_proc.barrier == barrier:
        return bar_proc
    return Barrier(bar_proc.barrier, bar_proc.proc.sync(barrier))

Barrier.sync = _barrier_sync


#   - $\text{sync}_B(\langle B \rangle P) = P$
#   
#   - $\forall C\neq B,~\text{sync}_B(\langle C \rangle P) = \langle C \rangle P$

# In[12]:


def _synchro_sync(sync_proc, barrier):
    if sync_proc.barrier != barrier:
        return sync_proc
    return sync_proc.proc

Synchro.sync = _synchro_sync


# ### Examples

# In[13]:


#print(proc1.proc.cont)


# In[14]:


#print(proc1.proc.cont.sync('B'))


# In[15]:


proc1_1 = par(Synchro('B', Term()),
            Prefix('start2', Synchro('B', Prefix('end2', Term()))),
            Synchro('B', Prefix('start3', Prefix('end3', Term()))))


# In[16]:


#print(proc1_1)


# In[17]:


#print(proc1_1.sync('B'))


# In[18]:


proc1_2 = par(Synchro('B', Term()),
            Synchro('B', Prefix('end2', Term())),
            Synchro('B', Prefix('start3', Prefix('end3', Term()))))


# In[19]:


#print(proc1_2)


# In[20]:


#print(proc1_2.sync('B'))


# ## Waiting

# $\text{wait}_B(0) = \text{false}$

# In[21]:


def _term_wait(proc, barrier):
    return False

Term.wait = _term_wait


# $\text{wait}_B(\alpha.P) = \text{wait}_B(P)$

# In[22]:


def _prefix_wait(prefix, barrier):
    return prefix.cont.wait(barrier)

Prefix.wait = _prefix_wait


# $\text{wait}_B(P \parallel Q) = \text{wait}_B(P) \lor \text{wait}_B(Q)$

# In[23]:


def _parallel_wait(proc, barrier):
    return proc.left.wait(barrier) or proc.right.wait(barrier)

Parallel.wait = _parallel_wait


# 
#   - $\text{wait}_B((\nu B)P) = \text{false}$
#   
#   - $\forall C\neq B,~\text{wait}_B((\nu C)P) = \text{wait}_B(P)$

# In[24]:


def _barrier_wait(bar_proc, barrier):
    if bar_proc.barrier == barrier:
        return False
    return bar_proc.proc.wait(barrier)

Barrier.wait = _barrier_wait


# 
#   - $\text{wait}_B(\langle B \rangle P) = \text{true}$
#   
#   - $\forall C\neq B,~\text{wait}_B(\langle C \rangle P) = \text{wait}_B(P)$

# In[25]:


def _synchro_wait(sync_proc, barrier):
    if sync_proc.barrier == barrier:
        return True
    return sync_proc.proc.wait(barrier)

Synchro.wait = _synchro_wait


# ### Examples

# In[26]:


#print(proc1)


# In[27]:


proc1.wait('B')


# In[28]:


#print(proc1.proc)


# In[29]:


proc1.proc.wait('B')


# ## Some "real" examples

# In[94]:


mips_example =      Barrier('end', Prefix('PC', 
    Barrier('next', 
      par(Prefix('PC_s', Synchro('next', Prefix('Next', Synchro('end', Term()))))
        , Prefix('Read', Barrier('alu', 
            par(Prefix('Sign', Synchro('alu', Term()))
              , Prefix('Ctrl', Prefix('Reg', Synchro('alu', Term())))
              , Synchro('alu', Prefix('ALU', par(Synchro('next', Term()),
                                                 Prefix('Ram', Prefix('WReg', Synchro('end', Term()))))))
              , Synchro('end', Prefix('End', Term())))))))))


# In[95]:


##print(mips_example)


# In[96]:


#display(Math(mips_example.to_latex()))


# In[101]:


# Remark: each barrier name could be associated to a "clock" so that they can be reused (e.g. in loops)
pargen_example =   Prefix("init", Barrier("gen1", Barrier("gen2",
    Prefix("gstart", par(Prefix("gcomp1", par(Synchro("gen1", Term()), Prefix("gcomp2", Synchro('gen2', Term()))))
                       , Prefix("step1", Barrier("io"
                       , par(Prefix("load", Prefix("xform", Synchro("io", Term())))
                            , Prefix("step2", Synchro("gen1", Prefix("step3", Synchro("io", Prefix("step4", 
                               Synchro("gen2", Prefix("end", Term())))))))))))))))


# In[102]:


##print(pargen_example)


# In[ ]:




