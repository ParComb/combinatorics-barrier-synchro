#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
from time import time
import os, random, signal, sys

import daglib
from ImportTaskDag import import_task_dag

from sage.all import gp

import RandLinExtSage as BIT
import random_gen_arch_generalise_v2 as Arch
import Huber, ForkJoin

from sage.all import factorial

class TimeoutException(Exception):
    pass

class BenchFJ:
    def __init__(self, graph):
        self.graph = graph
        self.pc = None # résultat des précalculs
        
    def count(self):
        return self.graph.count_le()
    
    def sample(self):
        return self.graph.sample_le()

class BenchBIT:

    def __init__(self, graph):
        self.graph = graph
        self.pc = None # résultat des précalculs

    def count(self):
        poly, integrals = BIT.compute_INTEGRALS(self.graph)
        self.pc = (integrals, poly.vars)

        return BIT.count_linear_extensions(poly)
        
    def sample(self):
        if self.pc is None:
            raise TimeoutException
        else:
            return BIT.sample_linear_extension(*self.pc)

class BenchHuber:
    def __init__(self, graph):
        self.graph = Huber.dag_to_poset(graph)
        self.pc = None

    def sample(self):
        return Huber.CFTP(self.graph)

class BenchArch:
    def __init__(self, graph):
        self.graph = graph
        self.pc = None
        self.T = dict()

    def count(self):
        Arch.T = self.T
        res = Arch.enum(self.graph[0])
        correction = Arch.coeff(self.graph[1])
        self.T = Arch.T
        return res/correction
        
    def sample(self):
        T = {}
        Arch.enum(self.graph[0])
        return Arch.alea_gen(Arch.fil(self.graph[0]), self.graph[0], len(self.graph[0])-1)
    
def handler(signum, frame):
    raise TimeoutException("Too much time spent")

def CountingRunner(benchs, timeout):

    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout)
   
    times = list()
    t = time()
    nb_le = 0
    try:
        for bench in benchs:
            nb_le += bench.count()
            times.append(time() - t)
            t = time()
            
    except Exception:
        pass
    signal.alarm(0)

    times.sort()

    if len(times) == 0:
        return (0,0,0,0,0,0)
        
    tmin = times[0]
    tmax = times[-1]
    tmed = times[len(times)//2]
    tavg = reduce(lambda a, b: a+b, times) / len(times)
    
    le_avg = nb_le / len(times)
    
    # avg le, nb handled, tmin, tmed, tmax, tavg
    return (le_avg, len(times), tmin, tmed, tmax, tavg)


def SamplingRunner(benchs, timeout, nb_samples=1):
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout)
   
    times = list()
    t = time()

    try:
        for bench in benchs:
            for _ in range(nb_samples):
                bench.sample()
            times.append(time() - t)
            t = time()
            
    except Exception as e:
        pass
    
    signal.alarm(0)

    if len(times) == 0:
        return (0,0,0,0,0)
    
    times = map(lambda x: x/nb_samples, times)
    times.sort()
    
    tmin = times[0]
    tmax = times[-1]
    tmed = times[len(times)//2]
    tavg = reduce(lambda a, b: a+b, times) / len(times)
        
    # nb handled, tmin, tmed, tmax, tavg
    return (len(times), tmin, tmed, tmax, tavg)

def TimeoutRunner(f, timeout, nb_samples):
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout)
   
    times = list()
    t = time()
    res = None

    try:
        for _ in range(nb_samples):
            res = f()
        times.append(time() - t)
        t = time()
            
    except Exception as e:
        res = None
    signal.alarm(0)
    
    return (res, times)

def timing(times, nb_samples):

    if len(times) == 0:
        return (0,0,0,0)
    
    times = map(lambda x: x/nb_samples, times)
    times.sort()
    
    tmin = times[0]
    tmax = times[-1]
    tmed = times[len(times)//2]
    tavg = reduce(lambda a, b: a+b, times) / len(times)
        
    # nb handled, tmin, tmed, tmax, tavg
    return (tmin, tmed, tmax, tavg)

def WholeRunner(benchs, timeout, nb_samples=1):

    counting_res = 0
    counting_size = 0
    counting_times = []
    sampling_times = []
    counting_handled = 0
    sampling_handled = 0
    
    for bench in benchs:
        c, t = TimeoutRunner(bench.count, timeout, 1)
        if not (c is None):
            counting_res += c
            counting_size += len(bench.graph)
            counting_handled += 1
            counting_times += t
            
        r, t = TimeoutRunner(bench.sample, timeout, nb_samples)
        if not (r is None):
            sampling_handled += 1
            sampling_times += t

    ctuple = (counting_res, counting_handled) + timing(counting_times, 1)
    stuple = (sampling_handled,) + timing(sampling_times, nb_samples)
    
    return ctuple, stuple
 
            
def timeout_run(bench, timeout):
    
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout)

    res = True
    
    try:
        bench()
    except Exception as e:
        res = None

    signal.alarm(0)
    return res
