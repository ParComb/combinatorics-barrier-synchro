
# import task dag
# cf.http://www.loria.fr/~suter/dags.html

class DAGTaskFileReader:
    def __init__(self, file_name):
        self.file = open(file_name, "r")

    def close(self):
        self.file.close()        

    def read(self, n):
        assert (n>0)
        first = self.file.read(1)
        while first=="#":
            self.file.readline() # skip the full comment line
            first = self.file.read(1)
            
        if n==1:
            return first
        else:
            return first + self.file.read(n-1)

    def read_word(self, word):
        rword = self.read(len(word))
        if rword != word:
            raise Exception("Incorrect word read: {0} (expected {1})".format(rword, word))
        return rword

    def read_until(self, end_ch="\n"):
        read_str = ""
        while True:
            ch = self.read(1)
            if ch == "" or ch == end_ch:
                return read_str
            else:
                read_str += ch

        return read_str
            
    def read_node_count(self):
        self.read_word("NODE_COUNT ")
        count_str = self.read_until("\n")
        # print("[DEBUG] Import node count = " + count_str)
        count = int(count_str)
        return count

    def read_node(self):
        self.read_word("NODE ")
        node_id = int(self.read_until(" "))
        children_list = self.read_until(" ")

        # print("[DEBUG] Import node={0} children={1}".format(node_id, children_list))

        if children_list == "-":
            children_ids = set()
        else:
            children_ids = { int(child) for child in children_list.split(",") }

        self.read_until("\n")

        return (node_id, children_ids)
        

def import_task_dag(file_name, dag):
    node_map = dict()

    reader = DAGTaskFileReader(file_name)
        
    node_count = reader.read_node_count()
    #print("[IMPORT] dag with {} vertices".format(node_count))

    for node in range(node_count):
        node_id, children_ids = reader.read_node()
        if node_id in node_map:
            src = node_map[node_id]
        else:
            src = dag.add_vertex()
        node_map[node_id] = src
        for child_id in children_ids:
            if child_id in node_map:
                dest = node_map[child_id]
            else:
                dest = dag.add_vertex()
                node_map[child_id] = dest

            dag.add_edge(src, dest)

    reader.close()
