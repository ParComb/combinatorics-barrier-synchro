#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, random

import daglib
from ImportTaskDag import import_task_dag

import RandLinExtSage as BIT
import random_gen_arch_generalise_v2 as Arch
import Huber, ForkJoin

from benchlib import *

random.seed(0xdeadbeef)
timeout = 100


dags = dict()

for filename in os.listdir("task_dags"):
    daglib.DAGVertex.NODE_ID = 0
    dag = daglib.DAG()
    import_task_dag("task_dags/" + filename, dag)
    dags[filename] = dag


print("""           ------ Task DAG ------
algo, size, #le, time
""")

for dag_name in sorted(dags.keys(), key=lambda x: len(dags[x])):
    dag = dags[dag_name]
    dag_size = len(dag)
    
    bhuber = [BenchHuber(dag)]
    bbit = [BenchBIT(dag)]
    
    cbit = CountingRunner(bbit, timeout)
    if cbit[-1] != 0:
        sbit = SamplingRunner(bbit, timeout)
        if sbit[-1] != 0:
           shuber = SamplingRunner(bhuber, timeout)

        print(dag_name + " :")
        print("BIT counting, {}, {}, {}".format(dag_size,cbit[0],cbit[-1]))
        print("BIT sampling, {}, {}, {}".format(dag_size,cbit[0],sbit[-1]))
        print("Huber sampling, {}, {}, {}\n".format(dag_size,cbit[0],shuber[-1]))

