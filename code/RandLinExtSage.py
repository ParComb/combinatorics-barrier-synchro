
import utils
import daglib

from sage.all import integral, var, factorial, gp, RR

from time import time

import random

# gp.set_precision(50)

VERBOSE = True
VERBOSE = False

class Poly:
    def __init__(self, poly=None,vars=None,integrals=None):
        if poly is None:
            self.poly = "1."
        else:
            self.poly = poly
        if vars is None:
            self.vars = set()
        else:
            self.vars = vars
        if integrals is None:
            self.integrals = []
        else:
            self.integrals = integrals
            
    def single_elimination(self, node, pred, succ):
        nvar, pvar, svar = 'N'+str(node.id) , 'N'+str(pred.id), 'N'+str(succ.id)

        nvars = self.vars | {nvar, pvar, svar}

        if VERBOSE:
            print("[Single({})]: integral{{{}, {}}}({}).d{}".format(nvar, svar, pvar, self.poly, nvar))

        npoly = "t"+nvar
        gp("{} = intformal({}, {})".format(npoly, self.poly, nvar))
        tpoly = "tt"+nvar
        gp("{} = subst({},{},{})-subst({},{},{})".format(tpoly, npoly, nvar, pvar, npoly, nvar, svar))
        
        if VERBOSE:
            print("         ==> {}\n".format(npoly))
            
        # INTEGRALS.append((npoly,nvar,svar,pvar))
        return Poly(poly=tpoly, vars=nvars, integrals = self.integrals+[(npoly,nvar,svar,pvar)])
        
    def root_elimination(self, node, succ):
        # global INTEGRALS
        
        nvar, svar = 'N'+str(node.id), 'N'+str(succ.id)
        nvars = self.vars | {nvar, svar}

        if VERBOSE:
            print("[Root({})]: integral{{{}, 1}}({}).d{}".format(nvar, svar, self.poly, nvar))


        npoly = "t"+nvar
        gp("{} = intformal({}, {})".format(npoly, self.poly, nvar))
        tpoly = "tt"+nvar
        gp("{} = subst({},{},1.)-subst({},{},{})".format(tpoly, npoly, nvar, npoly, nvar, svar))

        if VERBOSE:
            print("         ==> {}\n".format(npoly))
        # INTEGRALS.append((npoly,nvar,svar,"1."))

        return Poly(poly=tpoly, vars=nvars, integrals = self.integrals+[(npoly,nvar,svar,"1.")])

    def leaf_elimination(self, node, pred):
        # global INTEGRALS
        nvar, pvar = 'N'+str(node.id) ,'N'+str(pred.id)
        nvars = self.vars | {nvar, pvar}

        if VERBOSE:
            print("[Leaf({})]: integral{{0, {}}}({}).d{}".format(nvar, pvar, self.poly, nvar))

        npoly = "t"+nvar
        gp("{} = intformal({}, {})".format(npoly, self.poly, nvar))
        tpoly = "tt"+nvar
        gp("{} = subst({},{},{})-subst({},{},0.)".format(tpoly,npoly, nvar, pvar, npoly, nvar))



        
        if VERBOSE:
            print("         ==> {}\n".format(npoly))
        # INTEGRALS.append((npoly,nvar,"0.",pvar))

        return Poly(poly=tpoly, vars=nvars, integrals = self.integrals+[(npoly,nvar,"0.",pvar)])

    def alone_elimination(self, node):
        # global INTEGRALS
        nvar = 'N'+str(node.id)
        nvars = self.vars | {nvar}
        if VERBOSE:
            print("[Alone({})]: integral{{0, 1}}({}).d{}".format(nvar, self.poly, nvar))

        npoly = "t"+nvar
        gp("{} = intformal({}, {})".format(npoly, self.poly, nvar))
        tpoly = "tt"+nvar
        gp("{} = subst({},{},1.)-subst({},{},0.)".format(tpoly, npoly, nvar, npoly, nvar))


        if VERBOSE:
            print("         ==> {}\n".format(npoly))
            
        # INTEGRALS.append((npoly,nvar,"0.","1."))

        return Poly(poly=tpoly, vars=nvars, integrals = self.integrals+[(npoly,nvar,"0.","1.")])
        
    def __str__(self):
        return str(self.poly)
        
    def __repr__(self):
        return repr(self.poly)

def alone_reduction(poly,dag):
    alone = utils.set_take(dag.vertices)
    npoly = poly.alone_elimination(alone)
    dag.remove_vertex(alone)
    
    if dag.vertices:
        raise ValueError("Node not alone!")

    return npoly

def roots_reduction(poly,dag):
    roots = daglib.compute_roots_of_vertices(dag.vertices)
    reduced = False
    npoly = poly
    for root in roots:
        npoly = root_reduction(npoly, dag, root)
        reduced = True
    return reduced, npoly
    
def root_reduction(poly,dag,root):
    if VERBOSE:
       print("[REDUCTION] remove root N{}".format(root.id))
    npoly = poly.root_elimination(root, utils.set_take(root.nexts))
    dag.remove_vertex(root)

    return npoly

def leaves_reduction(poly,dag):
    leaves = daglib.compute_leaves_of_vertices(dag.vertices)
    reduced = False
    npoly = poly
    for leaf in leaves:
        npoly = leaf_reduction(npoly, dag, leaf)
        reduced = True
        
    return reduced, npoly

def leaf_reduction(poly,dag,leaf):
    if VERBOSE:
       print("[REDUCTION] remove leaf N{}".format(leaf.id))
    npoly = poly.leaf_elimination(leaf, utils.set_take(leaf.preds))
    dag.remove_vertex(leaf)

    return npoly

def singles_reduction(poly,dag):
    singles = daglib.compute_singles_of_vertices(dag.vertices)
    reduced = False
    npoly = poly
    for single in singles:
        npoly = single_reduction(npoly, dag, single)
        reduced = True
    return reduced, npoly
    
def single_reduction(poly,dag,single):
    if VERBOSE:
       print("[REDUCTION] remove single N{}".format(single.id))
    next = utils.set_take(single.nexts)
    pred = utils.set_take(single.preds)
    npoly = poly.single_elimination(single, pred , next)
    dag.add_edge(pred, next)
    dag.remove_vertex(single)

    return npoly

def compute_INTEGRALS(dag):
    # global INTEGRALS
    # INTEGRALS = list()
    dag_len = len(dag.vertices)
    reduced = True
    poly = Poly()
    while reduced:
        
        reduced = False

        red1 = dag.transitive_reduction()

        red2 = True
        while red2:
            red2 = False
            red2, poly = roots_reduction(poly, dag)

        red3 = True
        while red3:
            red3 = False
            red3, poly = leaves_reduction(poly, dag)

        red4, poly = singles_reduction(poly, dag)
        reduced = red4
        
    if len(dag.vertices) == 1:
        poly = alone_reduction(poly, dag)

    return poly, poly.integrals

def count_linear_extensions(poly):
    nb_le = gp(poly.poly +" * factorial("+str(len(poly.vars))+")").sage()
    
    return nb_le

def sample_linear_extension(integrals, vars):
    Xis = dict()
    ints = integrals[:]
    
    while len(ints) != 0:           
        f,x,a,b = ints.pop()
        to_subst = "["+",".join(Xis.keys())+"]"
        subs = "["+",".join(Xis.values())+"]"

        gp("f = substvec({},{},{})".format(f,to_subst,subs))
        
        a = a if not (a in Xis) else Xis[a]
        b = b if not (b in Xis) else Xis[b]
                
        gp("C = subst(f,{},{})-subst(f,{},{})".format(x, b, x, a))
        gp("U = random(1.)*({}-{})+{}".format(b,a,a))

        X = gp.eval("solvestep(t={},{},1.,subst(f,{},t)-subst(f,{},{})-U * C)[1]".format(a,b,x,x,a))
        Xis[x] = X

    Xis = {x : RR(v) for (x,v) in Xis.items()}
    return sorted(Xis, key=Xis.get, reverse=True)

def make_dag1():
    dag1 = daglib.DAG()
    daglib.DAGVertex.NODE_ID = int(0)
    N0 = dag1.add_vertex()
    N1 = dag1.add_vertex()
    N2 = dag1.add_vertex()
    N3 = dag1.add_vertex()
    N4 = dag1.add_vertex()
    N5 = dag1.add_vertex()
    N6 = dag1.add_vertex()
    dag1.add_edge(N0, N1)
    dag1.add_edge(N1, N2)
    dag1.add_edge(N1, N4)
    dag1.add_edge(N2, N3)
    dag1.add_edge(N4, N5)
    dag1.add_edge(N5, N3)
    dag1.add_edge(N3, N6)

    return dag1


if __name__ == "__main__":

    random.seed()
    
    dag1 = make_dag1()

    start_time = time()

    poly, integrals = compute_INTEGRALS(dag1)

    end_time = time()
    elapsed_time = end_time - start_time
    print("Precomputation time: {} s".format(elapsed_time))


    print(count_linear_extensions(poly))

    start_time = time()

    for i in range(10):
        print(sample_linear_extension(integrals, poly.vars))

    end_time = time()
    elapsed_time = end_time - start_time
    print("Sampling time: {} s".format(elapsed_time))
