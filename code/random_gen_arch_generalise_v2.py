#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function


### In the paper The Combinatorics of Barrier Synchronization
### we have studied the promise-processes by eliminating
### their arches from the first one to the last one.
### However, in this implementation the choice of treating
### them from the last one to the first one has been
### done for efficiency reason in the memoization.


import random, math, time, combi, daglib


### Partie verification pour debug

def check(A):
    # check if process A is well built : is a generalized Arch
    rep = True
    i=0
    while (i < len(A)-1) and rep:
        if (A[i][0] > A[i+1][0]) or (A[i][1] > A[i+1][1]):
            rep = False
        i += 1
    return len(A)<=1 or (rep and A[0][1]>=A[-1][0])


def find(L, l):
    # search for l in list L
    for i in range(len(L)):
        if L[i]==l:
            return i
    return -1


def check_lin_ext(A,L):
    # check if L is a linear extension of A
    rep = True
    i=0
    while (i < len(A)) and rep:
        d = find(L,'f_'+str(A[i][0]))
        f = find(L,'f_'+str(A[i][1]))
        a = find(L,'a_'+str(i))
        if not(d<a and a<f):
            print("pb : ",i)
            rep = False
        i += 1
    return rep
######


###  Sampling of Processes

def UnifGeneralArch(n,k):
    # n : total size
    # k : nb of arches (each one with multiple events)

    n -= 1  # we keep the final node for the final event of the process
    maxCombi = combi.binom(n-1,3*k-2)
    r = random.randint(0,maxCombi-1)
    C = combi.cons(n-1,3*k-2,r)    #compo(n,3k-1)
    C = C[:3*k-2]
    C = [-1]+[c-1 for c in C]+[n-1]
    A = []
    D = []
    for a in range(k):
        arc = (C[a]+1, C[k+a]+1)
        D.append(0)
        for l in range(C[-k+a]-C[-k-1+a]):
            A.append(arc)
            D[-1]+=1
    return A, D
        

def coeff(C):
    s = 1
    for c in C:
        s *= math.factorial(c)
    return s


def generalArchProc(n):
    # generate a random (almost) generalized
    #Arch process with n arches (non uniform)
    # a single event on each arch
    A = []
    for i in range(n):
        A.append((0,0))
    for i in range(1,n):
        A[i] = (A[i-1][0] + random.randint(0,n), A[i][1])
    A[0] = (A[0][0], A[n-1][0] + random.randint(0,n))
    for i in range(1,n):
        A[i] = (A[i][0], A[i-1][1] + random.randint(0,n))
    return A


def ArchProc(n,k):
    # generate an Arch process with n arches and k nodes inside the common part
    # corresponds to A(n+k, n) with notations of AOFA
    A = []
    for i in range(n):
        A.append((i,n+k+i))
    return A


def escalier2DAG(A,C):
    daglib.DAGVertex.NODE_ID = int(0)
    dag = daglib.DAG()
    N = []
    # n : nb nodes
    n = A[-1][1] + 1 + sum(C)
    for i in range(n):
        N.append(dag.add_vertex())
    for i in range(A[-1][1]):
        dag.add_edge(N[i], N[i+1])
    p = A[-1][1]
    for i in range(len(C)):
        p += 1
        dag.add_edge(N[A[sum(C[:i])][0]], N[p])
        for j in range(C[i]-1):
            dag.add_edge(N[p],N[p+1])
            p += 1
        dag.add_edge(N[p], N[A[sum(C[:i])][1]])
    return dag
           
#######


###  Enumeration on the nb of linear extensions
def fil(A):
    return ['f_'+str(i) for i in range(A[0][0], A[-1][1]+1)]


def increase(A):
    B = [A[0]] + A[:-1]
    return B

def extend(A):
    B = [(A[i][0],A[i][1]+1) for i in range(0,len(A)-1)]
    return B


def enum(A):
#    global NB
    global T, Mem
    
    tup = tuple(A)
    
    if Mem and (tup in T):
#        NB += 1
        return T[tup]

    if len(A) == 0:
        return 1
    
    elif len(A) == 1:
        if Mem:
            T[tup] = A[0][1]-A[0][0]
        else:
            return A[0][1]-A[0][0]
        
    elif A[0]==A[-1]:
        r = A[0][1]-A[0][0]
        s = 1
        for i in range(len(A)):
            s *= r+i
        if Mem:
            T[tup] = s
        else:
            return s
        
    else:
        if Mem:
            T[tup] =  (A[-1][1]-A[0][0] + len(A)-1)*enum(A[:-1]) - enum(increase(A)) + (A[0][1]-A[-1][0])*enum(extend(A))
        else:
            return  (A[-1][1]-A[0][0] + len(A)-1)*enum(A[1:]) - enum(increase(A)) + (A[0][1]-A[-1][0])*enum(extend(A))

    if Mem:
        return T[tup]
#########


### Sampling of runs

def shift(p,A):
    if len(A)==1:
        return []
    else:
        return [(b+(1 if p<=b else 0),e+(1 if p<=e else 0)) for (b,e) in A[:-1]]


def alea_gen(L, A, i):
    # generation de la fin vers le début
    # L fil de controle + actions déjà incluses,
    # A : arches restantes,
    # i valeur de l'indice de l'action de la premiere ligne
    # Mem : boolean : avec Memoization or not
    
    if len(A) == 0:
        return L

    r = random.randint(0, enum(A)-1)
    p = 1+A[-1][0]
    while r>=0:
        B = shift(p,A)
        e = enum(B)
        r -= e
        p += 1
        
    #r += e  #inutile
    p -= 1
    return alea_gen(L[:p]+['a_'+str(i)]+L[p:], B, i-1)

def couverture_tot(A):
    nb = 0
    e = enum(A)
    S = set()
    tm = time.time()
    while(len(S)<e):
        L=alea_gen(fil(A), A, len(A)-1)
        if not(check_lin_ext(A,L)):
            print("  pb  :  ",L)
        S.add(tuple(L))
        nb +=1
        
    print('couverture totale ',e,' runs; temps :',time.time()-tm, '; nb generations',nb)
    return 0

def preCalculs(A):
    global U
    if tuple(A) in U:
        return None
     
    if len(A) == 0:
        return None

    p = 1+A[-1][0]
    while p <= A[-1][1]:
        B = shift(p,A)
        e = enum(B)
        preCalculs(B)
        p += 1
    U.add(tuple(A))
    return None
#######


##### Test de couverture : temps et nb de générations
##Mem = True
##T = {}
##NB = 0
##n=4
##A = genArchProc(n,3)
##print(A, ":  A is a generalized Arch process ?  ",check(A))
##print("size of A  :  ", A[-1][1]+n)
##print('nb of linear extensions:   approx 10^',len(str(enum(A))),':  ',enum(A))
##print('taille memo', len(T))
##
##print('Avec MEMOiZATION : ')
##Mem = True
##T = {}
##couverture_tot(A)
##
##print('Sans MEMOiZATION : ')
##Mem = False
##T = {}
##couverture_tot(A)
##print('---')



######  Run sampling in generalized Arch processes with a single event on each arch
##Mem = True
##T = {}
##NB = 0
##n=50
##A = generalArchProc(n)
##print(A, ":  A is a generalized Arch process ?  ",check(A))
##print('nb of linear extensions:   approx 10^',len(str(A)),'  ',enum(A))
##print('taille memo', len(T))
##for i in range(3):
##    T={}
##    tmp = len(T)
##    NB = 0
##    tm = time.time()
##    L = alea_gen(fil(A), A, len(A)-1)
##    if not(check_lin_ext(A,L)):
##        print("  pb  :  ",L)
##    print("tps de gen",time.time()-tm, "nvelles valeurs memo", len(T)-tmp,"nb de hit dans memo", NB)#, len({k for k in dicoNB if dicoNB[k]>0}))
##print('---')


######  Run sampling in generalized Arch processes with SEVERAL events on each arch
######  in this context, the Couverture function must be adapted (if necessary)
##Mem = False   # Memoization or not
##T = {}
##NB = 0
##n = 30
##k = 5
##A,C = UnifGeneralArch(n,k)
##correction = coeff(C)
####  A is the process et C the composition of the arches
##print(A, ':  A is a generalized Arch process ?  ',check(A))
##print(C, ':  composition of the',k,' arches')
##print("size of A  :  ", n)
##d = escalier2DAG(A,C)
##d.to_dot_file('test_escalier.dot')
##print('nb of linear extensions:   approx 10^',len(str(enum(A)//correction)),'  ',enum(A)//correction)
##print('taille memo', len(T))
##print("Without Memoization")
##for i in range(5):
##    tmp = len(T)
##    NB = 0
##    tm = time.time()
##    L = alea_gen(fil(A), A, len(A)-1)
##    if not(check_lin_ext(A,L)):
##        print("  pb  :  ",L)
##    print("tps de gen",time.time()-tm, "nvelles valeurs memo", len(T)-tmp,"nb de hit dans memo", NB)#, len({k for k in dicoNB if dicoNB[k]>0}))
##print('---')
##print("With Memoization")
##Mem = True  # Memoization or not
##T = {}
##NB = 0
##enum(A)
##for i in range(5):
##    tmp = len(T)
##    NB = 0
##    tm = time.time()
##    L = alea_gen(fil(A), A, len(A)-1)
##    if not(check_lin_ext(A,L)):
##        print("  pb  :  ",L)
##    print("tps de gen",time.time()-tm, "nvelles valeurs memo", len(T)-tmp,"nb de hit dans memo", NB)#, len({k for k in dicoNB if dicoNB[k]>0}))
##print('---')


######  Run sampling in generalized Arch processes with SEVERAL events on each arch
######  in this context, the Couverture function must be adapted (if necessary)
##Mem = True   # Memoization or not
##T = {}
##NB = 0
##n = 200
##k = 50
####
##random.seed(987654321987654321)
####
##A,C = UnifGeneralArch(n,k)
##correction = coeff(C)
####  A is the process et C the composition of the arches
##print(A, ':  A is a generalized Arch process ?  ',check(A))
##print(C, ':  composition of the',k,' arches')
##print("size of A  :  ", n)
####d = escalier2DAG(A,C)
####d.to_dot_file('test_escalier.dot')
##print('nb of linear extensions:   approx 10^',len(str(enum(A)//correction)),'  ',enum(A)//correction)
##print('taille memo', len(T))
##for i in range(10):
##    tmp = len(T)
##  #  NB = 0
##    tm = time.time()
##    L = alea_gen(fil(A), A, len(A)-1)
##    if not(check_lin_ext(A,L)):
##        print("  pb  :  ",L)
##    print("tps de gen",time.time()-tm, "nvelles valeurs memo", len(T)-tmp,"nb de hit dans memo", NB)#, len({k for k in dicoNB if dicoNB[k]>0}))
##print('nb elts au total en memo : ',len(T))
##print('---')



######  Run sampling in generalized Arch processes with SEVERAL events on each arch
######  in this context, the Couverture function must be adapted (if necessary)
##Mem = True   # Memoization or not
##T = {}
##NB = 0
##n = 100
##k = 30
####
##random.seed(987654321987654321)
####
##A,C = UnifGeneralArch(n,k)
##correction = coeff(C)
####  A is the process et C the composition of the arches
##print(A, ':  A is a generalized Arch process ?  ',check(A))
##print(C, ':  composition of the',k,' arches')
##print("size of A  :  ", n)
####d = escalier2DAG(A,C)
####d.to_dot_file('test_escalier.dot')
##print('nb of linear extensions:   approx 10^',len(str(enum(A)//correction)),'  ',enum(A)//correction)
##print('taille memo', len(T))
##for i in range(10):
##    tmp = len(T)
##  #  NB = 0
##    tm = time.time()
##    L = alea_gen(fil(A), A, len(A)-1)
##    if not(check_lin_ext(A,L)):
##        print("  pb  :  ",L)
##    print("tps de gen",time.time()-tm, "nvelles valeurs memo", len(T)-tmp,"nb de hit dans memo", NB)#, len({k for k in dicoNB if dicoNB[k]>0}))
##print('nb elts au total en memo : ',len(T))
##print('---')
##print('relance des même calculs, mais avec memo sauvegardé, idem que pré-calculs')
####
##random.seed(987654321987654321)
####
##A,C = UnifGeneralArch(n,k)
##correction = coeff(C)
####  A is the process et C the composition of the arches
##print(A, ':  A is a generalized Arch process ?  ',check(A))
##print(C, ':  composition of the',k,' arches')
##print("size of A  :  ", n)
####d = escalier2DAG(A,C)
####d.to_dot_file('test_escalier.dot')
##print('nb of linear extensions:   approx 10^',len(str(enum(A)//correction)),'  ',enum(A)//correction)
##print('taille memo', len(T))
##for i in range(10):
##    tmp = len(T)
##  #  NB = 0
##    tm = time.time()
##    L = alea_gen(fil(A), A, len(A)-1)
##    if not(check_lin_ext(A,L)):
##        print("  pb  :  ",L)
##    print("tps de gen",time.time()-tm, "nvelles valeurs memo", len(T)-tmp,"nb de hit dans memo", NB)#, len({k for k in dicoNB if dicoNB[k]>0}))
##print('nb elts au total en memo : ',len(T))
##print('---')

if __name__ == '__main__':

    ####  Run sampling in generalized Arch processes with SEVERAL events on each arch
    ####  in this context, the Couverture function must be adapted (if necessary)
    Mem = True   # Memoization or not
    T = {}
    U = set()
    NB = 0
    n = 10
    k = 2
    ##
    # random.seed(0xdeadbeef)
    ##
    A,C = ([(0, 2), (0, 2), (1, 3), (1, 3), (1, 3), (1, 3)], [2, 4])
    # A,C = UnifGeneralArch(n,k)
    correction = coeff(C)
    ##  A is the process et C the composition of the arches
    print(A, ':  A is a generalized Arch process ?  ',check(A))
    print(C, ':  composition of the',k,' arches')
    print("size of A  :  ", n)
    tm = time.time()
    # preCalculs(A)
    # print('taille de la memo après pré-calculs complets : ',len(T), '   temps : ',time.time()-tm)
    # for i in range(50):
    #     tmp = len(T)
    #   #  NB = 0
    #     tm = time.time()
    #     L = alea_gen(fil(A), A, len(A)-1)
    #     if not(check_lin_ext(A,L)):
    #         print("  pb  :  ",L)
    #     print("tps de gen",time.time()-tm, "nvelles valeurs memo", len(T)-tmp,"nb de hit dans memo", NB)#, len({k for k in dicoNB if dicoNB[k]>0}))
    # print('nb elts au total en memo : ',len(T))
    # print('---')
    
    print(enum(A))
