
from Syntax import *
from Causal import *

from daglib import *

from LinextSage import count_linear_extensions

def causal_order_to_dag(order):
    dag = DAG()
    DAGVertex.NODE_ID = 1
    vert_map = dict()

    def add_vertex(overt):
        dvert = vert_map.get(overt)
        if dvert is None:
            dvert = dag.add_vertex()
            vert_map[overt] = dvert
        return dvert
    
    for osrc in order.vertices:
        dsrc = add_vertex(osrc)
        for odest in order.out_edges[osrc]:
            ddest = add_vertex(odest)
            dag.add_edge(dsrc, ddest)

    return dag

def count_linexts(proc):
    order = proc.causal_order()
    order.join_all()
    dag = causal_order_to_dag(order)
    is_cf, res = count_linear_extensions(dag)

    if is_cf:
        return res

    return None

def bench_proc(proc, descr):
    import timeit
    start_time = timeit.default_timer()
    proc_count = count_linexts(proc)
    elapsed = timeit.default_timer() - start_time
    print("{}: {} executions ({} ms)".format(descr, proc_count, elapsed))

if __name__ == "__main__":
    import timeit
    print("===== Counting ======")
    bench_proc(mips_example, "MIPS example")
    bench_proc(pargen_example, "Generator example")
