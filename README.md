# combinatorics-barrier-synchro

Companion site for the work "Combinatorics of barrier synchronization"

The repository contains:

- a companion document with some further technical developments, and also the proof details for some of the theorems stated in the submitted paper.

- the implementation of various counting and random generation algorithms, as well as a set of benchmark scripts. This is under the `core/` directory.

----
The site is Copyright (C) 2019 Bodini, Dien, Genitrini Peschanski.
Documents under the creative commons CC-BY-NC-SA (4.0).
Source code under the GPL 3.0.
